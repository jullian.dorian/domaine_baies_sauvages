<?php

namespace __DBS__Class;

use ISmallTable;

class Status implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    public function toArray(){

    }

    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_status WHERE id_status=:id", [':id'=>$id]);
        $result = $request->getResult();

        return new Status($result['id_status'], $result['libelle']);
    }

    public static function getAll()
    {
        global $database;
        $results = [];

        $request = $database->request("SELECT * FROM dbs_status", [], true);

        foreach ($request->getResult() as $item){
            array_push($results, new Status($item['id_status'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

}

class Disponible implements ISmallTable
{

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Disponible|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_disponible WHERE id_disponible=:id", [':id' => $id]);

        $item = $request->getResult();
        if (count($item) > 0)
            return new Disponible($item['id_disponible'], $item['libelle']);

        return null;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $returns = [];
        $request = $database->request("SELECT * FROM dbs_disponible", [], true);

        foreach ($request->getResult() as $item) {
            array_push($returns, new Disponible($item['id_disponible'], $item['libelle']));
        }

        return $returns;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}