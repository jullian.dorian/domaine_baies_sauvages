<?php

session_start();
require '__Database.php';

use __DBS__Database\Database;
use __User\User;

interface ISmallTable {

    /**
     * Return the class by the ID
     * @param $id
     * @return null
     */
    public static function getById($id);

    /**
     * Return an list of the
     * @return array|null
     */
    public static function getAll();
}

interface ISmallRelation{

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array);

}

class Visite {

    private $ip;
    private $date;

    public function __construct($ip, $date)
    {
        $this->ip = $ip;
        $this->date = $date;
    }

    /**
     * Retourne le nombre de visite dans la journée
     * @return int $nb_visite
     */
    public static function getVisiteToday()
    {

        global $database;

        $date = date('Y-m-d');

        $request = $database->request("SELECT count(*) as nb_visite FROM dbs_visite WHERE DAY(date_visite) = DAY(:_date)", [':_date'=>$date]);
        return $request->getResult()['nb_visite'];
    }

    /**
     * Retourne le nombre de visite dans le mois
     * @return int $nb_visite
     */
    public static function getVisiteMonth()
    {

        global $database;

        $date = date('Y-m-d');

        $request = $database->request("SELECT count(*) as nb_visite FROM dbs_visite WHERE MONTH(date_visite) = MONTH(:_date)", [':_date'=>$date]);
        return $request->getResult()['nb_visite'];
    }

    /**
     * Retourne le nombre de visite dans l'année
     * @return int $nb_visite
     */
    public static function getVisiteYear()
    {

        global $database;

        $date = date('Y-m-d');

        $request = $database->request("SELECT count(*) as nb_visite FROM dbs_visite WHERE YEAR(date_visite) = YEAR(:_date)", [':_date'=>$date]);
        return $request->getResult()['nb_visite'];
    }

    public function addVisiteToday()
    {
        global $database;

        if(!empty($this->ip)) {

            $request = $database->request("SELECT date_visite FROM dbs_visite WHERE ip_address = :ip ORDER BY date_visite DESC LIMIT 1", [':ip'=>$this->ip]);
            $result = $request->getResult()['date_visite'];

            if($result != $this->date) {
                $database->insert("dbs_visite", ['ip_address' => $this->ip, "date_visite" => $this->date, "token_gen" => generate_token()]);
            }
        }
    }

}

require '__Message.php';
require '__User.php';
require '__Picture.php';
require '__Wine.php';
require '__Item.php';
require '__Order.php';
require '__Class.php';

$maintenance = false;
$database = Database::getDefault();
$user = init();

/**
 * Génère un token de session
 * @return string
 * @throws Exception
 */
function generate_token() {

    $res = "";
    $max = 4;

    for($i = 0; $i < $max; $i++) {

        $res .= bin2hex(random_bytes(4));

        if($i < ($max-1)) $res .= "-";
    }

    return $res;
}

/**
 * Initialise le contenu du joueur et autre
 * @return User|null
 */
function init(){

    $date = date('Y-m-d');
    $ip = sha1(getRealIpAddress());

    $visite = new Visite($ip, $date);
    $visite->addVisiteToday();

    if(isset($_SESSION['user'])){
        $session = $_SESSION['user'];
        return User::fromArray($session);
    }
    return null;
}

function getRealIpAddress()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

if(!function_exists("redirect")){
    function redirect($page)
    {
        header('location: ' . $page);
        die();
    }
}

/**
 * Return if the user is an admin
 * @param User $user
 * @return bool
 */
function is_admin($user){
    return need_user() ? $user->isAdmin() : false;
}

/**
 * @return bool
 */
function need_user(){
    global $user;
    return !is_null($user);
}

function need_maintenance() {
    global $user, $maintenance;

    if($maintenance) {
        if($user == null) {
            redirect('/maintenance.php');
        }

        if(!$user->isAdmin()) {
            redirect('/maintenance.php');
        }
    }
}