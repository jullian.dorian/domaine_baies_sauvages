<?php

namespace __DBS__Database;

/**
 * Class Database
 * @package __DBS__Class
 */
class Database{

    private $loader;
    private $host;
    private $database;
    private $port;
    private $user;
    private $password;
    private $args;

    private $connected = false;
    /** @var null|\PDO */
    private $pdo = null;

    public function __construct($loader, $host, $database, $port, $user, $password, $args = "")
    {
        $this->loader = $loader;
        $this->host = $host;
        $this->database = $database;
        $this->port= $port;
        $this->user = $user;
        $this->password = $password;
        $this->args = $args;
    }

    /**
     * Create the default database
     * @return Database
     */
    public static function getDefault()
    {
        $database = new Database("mysql", "localhost", "baies", 3306, "root", "");
        $database->start();
        return $database;
    }

    /**
     * Put a request on the database, return the result fetch or fetchAll
     * @param string $req
     * @param array|null $params
     * @param bool $fetchAll
     * @return null|Request
     */
    public function request($req, $params = [], $fetchAll = false)
    {
        if($this->isConnected()){

            $stmt = $this->pdo->prepare($req);
            foreach ($params as $t => &$v)
                $stmt->bindParam($t, $v);

            $request = new Request($stmt);
            $request->setType(TypeRequest::SELECT);
            $request->setFetchMode($fetchAll);
            $request->execute();

            $stmt->closeCursor();

            return $request;
        } else {
            echo "La base de données n'est pas connectée.";
            return null;
        }
    }

    /**
     * @param string $table
     * @param array $params
     * @return Request
     */
    public function insert($table, $params = [])
    {
        if($this->isConnected()){

            $args = "";
            $int = "";
            $values = [];

            $i = count($params);
            foreach ($params as $key=>$value){
                if($value != null) {
                    $args .= "`" . $key . "`";
                    array_push($values, $value);
                    $int .= "?";
                    $i--;
                    if ($i != 0) {
                        $args .= ",";
                        $int .= ",";
                    }
                } else {
                    $i--;
                }
            }

            $stmt = $this->pdo->prepare("INSERT INTO ".$table."(" . $args . ") VALUE (" . $int . ")");
            for ($i = 0; $i < count($values); $i++){
                $stmt->bindValue($i+1, $values[$i]);
            }

            $request = new Request($stmt);
            $request->setType(TypeRequest::INSERT);
            $request->execute();
            $request->setLastId($this->pdo->lastInsertId());

            $stmt->closeCursor();

            return $request;
        } else {
            die("La base de données n'est pas connectée.");
        }
    }

    /**
     * @param $table
     * @param array $params
     * @param array $where
     * @return Request
     */
    public function update($table, $params = [], $where = [])
    {
        if($this->isConnected()){

            $statement = "UPDATE $table SET ";

            $i = sizeof($params);
            foreach ($params as $key=>$value) {
                if(empty($value) && is_bool($value)) {
                    $value = ($value == true ? 1 : 0);
                }

                $statement .= (is_null($value) ? "$key=NULL" : "$key=$value");
                $i--;
                if($i != 0){
                    $statement .= ", ";
                }
            }

            $statement .= " WHERE ";

            $i = sizeof($where);
            foreach ($where as $key=>$value) {
                $statement .= "$key=$value";
                $i--;
                if($i != 0){
                    $statement .= " AND ";
                }
            }

            $stmt = $this->pdo->prepare($statement);

            $request = new Request($stmt);
            $request->setType(TypeRequest::UPDATE);
            $request->execute();

            return $request;
        } else {
            die("La base de données n'est pas connectée.");
        }
    }

    /**
     * @param $table
     * @param array $params
     * @return Request|null
     */
    public function delete($table, $params = [])
    {
        if($params == [] || sizeof($params) == 0)
            return null;

        if($this->isConnected()){

            $args = "";

            $count = sizeof($params);
            if($count>0) $args = " WHERE ";


            foreach ($params as $key=>$value) {
                $args .= $key . "=" . $value;
                $count--;
                if($count != 0)
                    $args .= " AND ";
            }

            $stmt = $this->pdo->prepare("DELETE FROM " . $table . $args);

            $request = new Request($stmt);
            $request->setType(TypeRequest::DELETE);
            $request->execute();

            $stmt->closeCursor();

            return $request;
        } else {
            die("La base de données n'est pas connectée.");
        }
    }

    /**
     * Start the connexion PDO
     * @return \PDO
     */
    protected function start()
    {
        $pdo = null;

        try{
            $dsn = ($this->loader . ":host=" . $this->host . ";dbname=" . $this->database . ";charset=utf8");
            $pdo = new \PDO($dsn, $this->user, $this->password);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $this->connected = true;
            $this->pdo = $pdo;
        } catch (\PDOException $e){
            die($e->getMessage());
        }

        return $pdo;
    }

    /**
     * Stop the database
     */
    public function stop()
    {
        $this->connected = false;
        $this->pdo = null;
    }

    /**
     * Check if the database is connected
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->connected;
    }

}

/**
 * Class Request
 * @package __DBS__Class
 */
class Request{

    /** @var \PDOStatement $request */
    private $request;
    /** @var bool $execute */
    private $execute;
    /** @var bool $fetchMode */
    private $fetchMode;
    /** @var array $result */
    private $result;
    /** @var int $lastId */
    private $lastId;
    /** @var int rowCount */
    private $rowCount;

    /** @var int type */
    private $type;

    public function __construct($request)
    {
        $this->request = $request;
        $this->execute = false;
        $this->fetchMode = false;
        $this->result = array();
    }

    /**
     * Apply the statement execute on the param to set the execute request
     */
    public function execute()
    {
        $this->execute = $this->request->execute();
        if($this->type == TypeRequest::SELECT || $this->type == TypeRequest::CALL){
            $this->rowCount = $this->request->rowCount();
            $this->result();
        }

        $this->request->closeCursor();
    }

    /**
     * Place the statement on param, set the result on fetch within the fetchMode
     */
    private function result()
    {
        $this->result = ($this->fetchMode ? $this->request->fetchAll() : $this->request->fetch());
    }

    /**
     * Check if the request has been success
     * @return bool
     */
    public function isSuccess()
    {
        return $this->execute;
    }

    /**
     * Set the fetchMode request, true = fetchAll, false = fetch
     * @param bool $mode
     */
    public function setFetchMode($mode)
    {
        $this->fetchMode = $mode;
    }

    /**
     * Get the last ID added
     * @param $lastId
     */
    public function setLastId($lastId)
    {
        $this->lastId = $lastId;
    }

    /**
     * Return the lastId of the request
     * @return int|null
     */
    public function getLastId(): int
    {
        if($this->type == TypeRequest::INSERT)
            return $this->lastId;
        else
            return null;
    }

    /**
     * Set the type of the request
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }

    /**
     * Return the rowCount of the request
     * @return int
     */
    public function getRowCount(): int
    {
        return $this->rowCount;
    }

    /**
     * Retourne le nombre de résultat
     * @return int
     */
    public function getCount(): int
    {
        return sizeof($this->getResult());
    }

    /**
     * Return the result of the request
     * @return array|null
     */
    public function getResult()
    {
        if($this->type == TypeRequest::SELECT || $this->type == TypeRequest::CALL)
            return $this->result;
        else
            return null;
    }

}

/**
 * Enum Class TypeRequest
 * @package __DBS__Class
 */
class TypeRequest{
    const SELECT = 0;
    const UPDATE = 1;
    const DELETE = 2;
    const INSERT = 3;
    const CALL = 4;
}

?>