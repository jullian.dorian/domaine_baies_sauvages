<?php

namespace __Item;

use __DBS__Class\Disponible;
use __Picture\Picture;
use __Wine\Conditionnement;
use __Wine\Wine;
use ISmallTable;

class Item implements ISmallTable {

    private $id;
    protected $token;
    protected $libelle;
    protected $description;
    protected $details;
    protected $price;
    protected $promotion;
    protected $stock;
    protected $id_wine;
    protected $id_cond;
    protected $id_picture;
    protected $id_dispo;
    protected $litre;
    protected $show;
    protected $date;

    public function __construct($id, $token, $libelle, $description, $details, $price, $promo, $stock, $litre, $idw, $idc, $idp, $idd, $show, $date)
    {
        $this->id = $id;
        $this->token = $token;
        $this->libelle = $libelle;
        $this->description = $description;
        $this->details = $details;
        $this->price = $price;
        $this->promotion = $promo;
        $this->stock = $stock;
        $this->litre = $litre;
        $this->id_wine = $idw;
        $this->id_cond = $idc;
        $this->id_picture = $idp;
        $this->id_dispo= $idd;
        $this->show = $show;
        $this->date = $date;
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Item|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_item WHERE id_item=:id", [":id"=>$id]);

        $item = $request->getResult();
        if(count($item)>0) {
            return new Item($item['id_item'], $item['token'], $item['libelle'],
                $item['description'], $item['details'], $item['price'], $item['promotion'],
                $item['stock'], $item['litre'], $item['id_wine'], $item['id_conditionnement'],
                $item['id_picture'], $item['id_disponible'], $item['show'], $item['date_creation']);
        }
        return null;
    }

    public static function getByToken($token)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_item WHERE token=:token", [":token"=>$token]);

        $item = $request->getResult();

        if($item !== false) {
            return new Item($item['id_item'], $item['token'], $item['libelle'],
                $item['description'], $item['details'], $item['price'], $item['promotion'],
                $item['stock'], $item['litre'], $item['id_wine'], $item['id_conditionnement'],
                $item['id_picture'], $item['id_disponible'], $item['show'], $item['date_creation']);
        }
        return null;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $returns = [];
        $request = $database->request("SELECT * FROM dbs_item", [], true);

        foreach ($request->getResult() as $item) {
            array_push($returns, new Item($item['id_item'], $item['token'], $item['libelle'],
                $item['description'], $item['details'], $item['price'], $item['promotion'],
                $item['stock'], $item['litre'], $item['id_wine'], $item['id_conditionnement'],
                $item['id_picture'], $item['id_disponible'], $item['show'], $item['date_creation']));
        }

        return $returns;
    }

    public static function getLast($max = 1, $order = "DESC")
    {
        global $database;

        $returns = [];
        $request = $database->request("SELECT * FROM dbs_item WHERE id_picture is not null ORDER BY date_creation $order LIMIT $max", [], ($max != 1));

        if($max == 1 && count($request->getResult())>0) {
            $item = $request->getResult();

            return new Item($item['id_item'], $item['token'], $item['libelle'],
                $item['description'], $item['details'], $item['price'], $item['promotion'],
                $item['stock'], $item['litre'], $item['id_wine'], $item['id_conditionnement'],
                $item['id_picture'], $item['id_disponible'], $item['show'], $item['date_creation']);
        }

        foreach ($request->getResult() as $item) {
            array_push($returns, new Item($item['id_item'], $item['token'], $item['libelle'],
                $item['description'], $item['details'], $item['price'], $item['promotion'],
                $item['stock'], $item['litre'], $item['id_wine'], $item['id_conditionnement'],
                $item['id_picture'], $item['id_disponible'], $item['show'], $item['date_creation']));
        }

        return $returns;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @return mixed
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @return Wine
     */
    public function getWine()
    {
        return Wine::getById($this->id_wine);
    }

    /**
     * @return Conditionnement
     */
    public function getConditionnement()
    {
        return Conditionnement::getById($this->id_cond);
    }

    /**
     * @return Picture
     */
    public function getPicture()
    {
        return Picture::getById($this->id_picture);
    }

    /**
     * @return mixed
     */
    public function isShow()
    {
        return $this->show;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getLitre()
    {
        return $this->litre;
    }

    /**
     * @return Disponible
     */
    public function getDisponible()
    {
        return Disponible::getById($this->id_dispo);
    }

}

class RegisterItem extends Item {

    public function __construct($token, $libelle, $description, $details, $price, $promo, $stock, $idw, $idc, $idp, $date)
    {
        parent::__construct(-1, $token, $libelle, $description, $details, $price, $promo, $stock, $idw, $idc, $idp, true, $date);
    }

    /**
     * Créer et sauvegarde l'item dans la base de données
     * @return bool
     */
    public function create():bool
    {
        global $database;

        $request = $database->insert("dbs_item",
            ["token"=>$this->token, "libelle"=>$this->libelle, "description"=>$this->description,
                "details"=>$this->details,"promotion"=>$this->promotion,"stock"=>$this->stock,"price"=>$this->price,
                'id_wine'=>$this->id_wine, 'id_conditionnement'=>$this->id_cond, 'id_picture'=>$this->id_picture,
                'date_creation'=>$this->date]);

        return $request->isSuccess();
    }

}