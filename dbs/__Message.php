<?php

namespace __Message;

class Message {

    const MESSAGES = [
        'inscription' => [
            "EMAIL_EXIST"=> "L'adresse email existe déjà.",
            "PASSWORD_NOT_MATCH"=> "Les mots de passes ne correspondent pas."
        ],
        'connection' => [
            "EMAIL_PASS_INCORRECT" => "L'adresse email ou le mot de passe est incorrecte."
        ],
        'categorie' => [
            "FIELD_CREATED" => "Le libellé a bien été crée.",
            "FIELD_CREATE_FAILED" => "Impossible de créer le libellé dans cette catégorie.",
            "FIELD_REMOVED" => "Le libellé a bien été supprimé.",
            "FIELD_REMOVE_FAILED" => "Impossible de supprimer le libellé dans cette catégorie."
        ],
        'wine' => [
            "WINE_CREATED" => "Le vin a bien été crée.",
            "WINE_CREATE_ERROR" => "Impossible de créer le vin.",
            "WINE_REMOVED" => "Le vin a bien été supprimée.",
            "WINE_REMOVE_ERROR" => "Impossible de supprimer le vin.",
            "ERROR_IGP" => "Merci de séléctionner un IGP valide."
        ],
        'user' => [
            "USER_CHANGED" => "L'utilisateur a bien été modifié.",
            "USER_CHANGE_ERROR"=>"Impossible de modifier l'utilisateur."
        ],
        'image' => [
            "UPLOAD_FILE"=> "Les images ont bien été envoyés.",
            "UPLOAD_FILE_ERROR"=> "Impossible d'enregistrer les images.",
            "DELETE_FILE"=>"L'image a bien été supprimé.",
            "DELETE_FILE_ERROR"=>"Impossible de supprimer l'image.",
        ],
        'diaporama' => [
            "RESET_DIAPO" => "Le diaporama a bien été reset.",
            "RESET_DIAPO_ERROR" => "Impossible de reset le diaporama.",
            "ADD_PICTURE"=> "Vous avez ajouté une image au diaporama.",
            "ADD_PICTURE_ERROR"=>"Impossible d'ajouter une image au diaporama.",
            "CHANGE_VISIBILITY"=>"Vous avez changé la visibilité du diaporama.",
            "CHANGE_VISIBILITY_ERROR"=>"Impossible de changer la visibilité."
        ],
        'article'=> [
            "ADD_ITEM"=>"L'article a bien été crée.",
            "ADD_ITEM_ERROR"=>"Impossible de créer l'article."
        ],
        'group' => [
            "CREATE_GROUP" => "Le groupe a bien été crée.",
            "CREATE_GROUP_ERROR" => "Impossible de créer le groupe"
        ]
    ];

    /**
     * Retourne un message selon l'id
     * @param string $page
     * @param int $id
     * @param null|array $args
     * @return string
     */
    public static function findById($page, $id, $args = null)
    {
        $message = null;

        foreach (Message::MESSAGES as $key=>$value) {
            if($page == $key) {
                $message['type'] = "error";

                $i = 0;
                foreach ($value as $msg) {
                    if($i == ($id-1)){
                        $message['msg'] = $msg;
                        break;
                    }
                    $i++;
                }
                break;
            }
        }

        if($args != null) {
            foreach ($args as $key => $value) {
                $message['msg'] = str_replace($key, $value, $message['msg']);
            }
        }

        return $message;
    }

}