<?php

namespace __Order;

use __User\User;
use ISmallTable;

class Order{

    private $id;
    private $token;
    private $date_creation;
    private $id_user;
    private $id_status;
    private $id_paiement;
    private $id_transport;

    private $articles;

    public function __construct($id, $token, $date_creation, $id_user, $id_status, $id_paiement, $id_transport)
    {
        $this->id = $id;
        $this->token = $token;
        $this->date_creation = $date_creation;
        $this->id_user = $id_user;
        $this->id_status = $id_status;
        $this->id_paiement = $id_paiement;
        $this->id_transport = $id_transport;
    }

    /**
     * Retourne une Commande
     * @param $id
     * @return Order|null
     */
    public static function getById($id)
    {
        global $database;
        $result = null;
        $request = $database->request("SELECT * FROM dbs_order WHERE id_order=:id", [':id'=>$id]);

        $item = $request->getResult();
        $result = new Order($item['id_order'], $item['token'], $item['date_creation'],$item['id_user'],$item['id_status'],
            $item['id_paiement'],$item['id_transport']);

        return $result;
    }

    /**
     * Retourne une liste de commande selon l'utilisateur
     * @param mixed $user
     * @return array|Order
     */
    public static function getByUser($user)
    {
        $id = is_int($user) ? $user : $user->getId();

        global $database;
        $results = [];
        $request = $database->request("SELECT * FROM dbs_order WHERE id_user=:id ORDER BY id_order desc", [':id'=>$id], true);

        foreach ($request->getResult() as $item){
            $order = new Order($item['id_order'],$item['token'],  $item['date_creation'],$item['id_user'],$item['id_status'],
                $item['id_paiement'],$item['id_transport']);


            $request = $database->request("SELECT quantity, id_item FROM dbs_contains WHERE id_order=:id",[':id'=>$item['id_order']], true);

            foreach ($request->getResult() as $item2) {
                $order->addArticle($item2['quantity'], $item['id_item']);
            }
            array_push($results, $order);
        }


        return $results;
    }

    public static function getByToken($token)
    {
        global $database;
        $results = [];
        $request = $database->request("SELECT * FROM dbs_order WHERE token=:token ORDER BY id_order desc", [':token'=>$token], true);

        foreach ($request->getResult() as $item){
            $order = new Order($item['id_order'],$item['token'],  $item['date_creation'],$item['id_user'],$item['id_status'],
                $item['id_paiement'],$item['id_transport']);


            $request2 = $database->request("SELECT quantity, id_item FROM dbs_contains WHERE id_order=:id",[':id'=>$item['id_order']], true);

            foreach ($request2->getResult() as $item2) {
                $order->addArticle($item2['quantity'], $item['id_item']);
            }
            array_push($results, $order);
        }


        return $results;
    }

    private function addArticle($qtt, $item) {
        $this->articles[] = ['quantity' => $qtt, 'id_item'=>$item];
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Retourne la liste des Commandes
     * @return array|Order
     */
    public static function getAll()
    {
        global $database;
        $results = [];
        $request = $database->request("SELECT * FROM dbs_order ORDER BY id_order DESC ", [], true);

        foreach ($request->getResult() as $item){
            array_push($results, new Order($item['id_order'], $item['token'], $item['date_creation'],$item['id_user'],$item['id_status'],
                $item['id_paiement'],$item['id_transport']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::getById($this->id_user);
    }
}

class Paiement implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Paiement|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_paiement WHERE id_paiement=:id", [':id' => $id]);
        $result = $request->getResult();

        return new Paiement($result['id_paiement'], $result['libelle']);
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;
        $results = [];

        $request = $database->request("SELECT * FROM dbs_paiement", [], true);

        foreach($request->getResult() as $item){
            array_push($results, new Paiement($item['id_paiement'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

