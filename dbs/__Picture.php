<?php

namespace __Picture;

use ISmallTable;

class Diaporama implements ISmallTable {

    private $id_diapo;
    private $id_picture;
    private $show;

    public function __construct($id, $id_picture, $show = false)
    {
        $this->id_diapo = $id;
        $this->id_picture = $id_picture;
        $this->show = $show;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return ['id_diapo'=>$this->id_diapo, 'id_picture'=>$this->id_picture, 'show'=>$this->show];
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Diaporama|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_diaporama WHERE id_diapo=:id", [':id'=>$id]);
        $result = $request->getResult();

        if(count($result)>0) {
            return new Diaporama($result['id_diapo'], $result['id_picture'], $result['show']);
        }

        return null;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $returns = [];

        $request = $database->request("SELECT * FROM dbs_diaporama", [], true);

        foreach ($request->getResult() as $result) {
            array_push($returns, new Diaporama($result['id_diapo'], $result['id_picture'], $result['show']));
        }

        return $returns;
    }

    /**
     * Reset le diaporama en mettant l'image a null  et le show a false
     * @return bool
     */
    public function reset()
    {
        global $database;

        $request = $database->update("dbs_diaporama", ['id_picture'=>null,'`show`'=>false], ['id_diapo'=>$this->id_diapo]);

        return $request->isSuccess();
    }

    public function changeVisibility()
    {
        global $database;

        $request = $database->update("dbs_diaporama", ['`show`'=>!$this->show], ['id_diapo'=>$this->id_diapo]);

        return $request->isSuccess();
    }

    public function updatePicture()
    {
        if($this->id_picture <= 0)
            return false;

        global $database;

        $request = $database->update("dbs_diaporama", ['id_picture'=>$this->id_picture, '`show`'=>true], ['id_diapo'=>$this->id_diapo]);

        return $request->isSuccess();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id_diapo;
    }

    /**
     * @return null|Picture
     */
    public function getPicture()
    {
        return ($this->id_picture != null ? Picture::getById($this->id_picture) : null);
    }

    /**
     * @param int $id_picture
     */
    public function setPicture($id_picture)
    {
        $this->id_picture = $id_picture;
    }

    /**
     * @return bool
     */
    public function isShow(): bool
    {
        return $this->show;
    }
}

class Picture implements ISmallTable {

    private $id;
    private $name;
    private $group;
    private $url;
    private $date;

    public function __construct($id, $name, $group, $url, $date)
    {
        $this->id = $id;
        $this->name = $name;
        $this->group = $group;
        $this->url = $url;
        $this->date = $date;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Picture|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_picture WHERE id_picture=:id",[':id'=>$id]);
        $result = $request->getResult();

        if(sizeof($result)>0) {
            return new Picture($result['id_picture'], $result['name'], $result['group'],
                $result['full_url'], $result['date_creation']);
        }
        return null;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $returns = [];
        $request = $database->request("SELECT * FROM dbs_picture ORDER BY `group` ASC",[], true);

        $last_group = null;

        foreach ($request->getResult() as $result) {
            if($last_group == null || $last_group != $result['group']) {
                $last_group = $result['group'];
                $returns[$last_group] = [];
            }


            array_push($returns[$last_group], new Picture($result['id_picture'],
                $result['name'], $result['group'], $result['full_url'], $result['date_creation']));

        }

        return $returns;
    }

    /**
     * @return bool
     */
    public function delete()
    {
        global $database;

        $absolute_file = $_SERVER['DOCUMENT_ROOT'] . "/assets/upload/" . $this->group . "/" . $this->name;
        chmod($absolute_file, 0777);
        if(unlink($absolute_file)) {
            $request = $database->delete("dbs_picture", ['id_picture'=>$this->id]);

            return $request->isSuccess();
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

}

class RegisterPicture {

    private $name;
    private $group;
    private $url;
    private $date;

    public function __construct($name, $group, $url, $date)
    {
        $this->name = $name;
        $this->url = $url;
        $this->date = $date;
        $this->group = $group;
    }

    /**
     * Enregistre l'image dans la base de données
     * @return bool
     */
    public function create(): bool
    {
        global $database;

        $request = $database->insert('dbs_picture', ['name'=>$this->name, 'group'=>$this->group, 'full_url'=>$this->url, 'date_creation'=>$this->date]);

        return $request->isSuccess();
    }

}