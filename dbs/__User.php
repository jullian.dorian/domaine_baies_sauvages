<?php

namespace __User;

class User {

    /** @var int $id */
    private $id;
    /** @var string $email */
    private $email;
    /** @var string $password */
    private $password;
    /** @var string $name */
    private $name;
    /** @var string $surname */
    private $surname;
    /** @var int $age */
    private $age;
    /** @var string $telephone */
    private $telephone;
    /** @var string $address */
    private $address;
    /** @var bool $client */
    private $client;
    /** @var bool $admin */
    private $admin;
    private $date_creation;
    private $token;


    /**
     * User constructor.
     * @param int $id
     * @param string $email
     * @param string $password
     * @param string $name
     * @param string $surname
     * @param int $age
     * @param string $telephone
     * @param string $address
     * @param bool $client
     * @param bool $admin
     * @param $date_creation
     * @param string $token
     */
    public function __construct($id, $email, $password, $name, $surname, $age, $telephone, $address, $client, $admin, $date_creation, $token)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->telephone = $telephone;
        $this->address = $address;
        $this->client = $client;
        $this->admin = $admin;
        $this->date_creation = $date_creation;
        $this->token = $token;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_user'=>$this->id, 'email' => $this->email, 'password' => null,
            'name'=>$this->name, 'surname'=>$this->surname, 'age'=>$this->age, 'telephone'=>$this->telephone,
            'address'=>$this->address, 'client'=>$this->client, 'admin'=>$this->admin, 'date_creation'=>$this->date_creation,
            'token' => $this->token);
    }

    /**
     * Return the user class from an array
     * @param $array
     * @return User
     */
    public static function fromArray($array)
    {
        $user = new User($array['id_user'], $array['email'], $array['password'],
            $array['name'],$array['surname'],$array['age'],$array['telephone'],$array['address'],
            $array['client'],$array['admin'],$array['date_creation'], $array['token']);
        return $user;
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return User|null
     */
    public static function getById($id)
    {
        global $database;

        if($database->isConnected()){

            $user = null;
            $request = $database->request("SELECT * FROM dbs_user WHERE id_user=:id", array(':id'=>$id));
            $request->execute();

            if($request->getRowCount() == 1)
                $user = self::fromArray($request->getResult());

            return $user;
        } else {
            die("Impossible de se connecter à la base de données.");
        }
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        if($database->isConnected()){

            $user = null;
            $request = $database->request("SELECT * FROM dbs_user", [], true);
            $request->execute();

            $results = [];

            foreach ($request->getResult() as $result){
                $user = self::fromArray($result);
                array_push($results, $user);
            }

            return $results;
        } else {
            die("Impossible de se connecter à la base de données.");
        }
    }

    /**
     * @param string $to
     * @return bool
     */
    public function changeTo($to = "user") {
        global $database;
        if($to == "admin" || $to == "user") {
            $request = $database->update("dbs_user", ["admin" => ($to == "admin")], ["id_user" => $this->id]);
            return $request->isSuccess();
        } else {
            return false;
        }
    }

    /**
     * Sauvegarde la session actuel de l'utilisateur
     */
    public function saveSession()
    {
        $_SESSION['user'] = $this->toArray();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Return if the user is an admin (true) or not (false)
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @return bool
     */
    public function isClient(): bool
    {
        return $this->client;
    }
}

class LoginUser {

    private $email;
    private $password;

    private $user;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Vérifie si l'utilisateur peut se connecter
     * @return bool
     */
    public function exist()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_user WHERE email=:email AND password=:passw",
            [':email' => $this->email, ':passw' => $this->password], false);
        $request->execute();

        if(sizeof($request->getResult()) > 0){

            $this->user = User::fromArray($request->getResult());

            return true;
        }

        return false;
    }

    /**
     * Retourne le user
     * @return User
     */
    public function connect()
    {
        return $this->user;
    }

}

class RegisterUser {

    private $email;
    private $password;
    private $password2;
    private $name;
    private $surname;
    private $age;
    private $address;
    private $telephone;
    private $client;
    private $token;

    public function __construct($email, $password, $password2, $name, $surname, $age, $address, $telephone, $client)
    {
        $this->email = $email;
        $this->password = sha1($password);
        $this->password2 = sha1($password2);
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->address = $address;
        $this->telephone = $telephone;
        $this->client = $client;
        $this->token = generate_token();
    }

    /**
     * Checks if the password are equals
     * @return bool
     */
    public function passwordEquals()
    {
        return $this->password == $this->password2;
    }

    /**
     * @return bool
     */
    public function exist()
    {
        global $database;

        $request = $database->request("SELECT count(email) as count FROM dbs_user
                  WHERE email=:email", [':email'=>$this->email]);
        $request->execute();
        $c = $request->getResult()['count'];

        return $c >= 1;
    }

    /**
     * Créer l'utilisateur, l'enregistre dans la BDD et retourne l'utilisateur
     * @return User;
     */
    public function create()
    {
        global $database;

        $request = $database->insert("dbs_user",
            ['email' => $this->email, 'password'=>$this->password, 'name'=>$this->name,'surname'=>$this->surname,
                'age'=>$this->age, 'telephone'=>$this->telephone,'address'=>$this->address, 'client'=>($this->client == true ? 1 : 0),
                'admin'=>0, 'date_creation'=>date('Y-m-d')]);

        $id = $request->getLastId();

        return new User($id, $this->email, $this->password, $this->name, $this->surname, $this->age, $this->telephone,
            $this->address, $this->client, false, date('Y-m-d'), $this->token);
    }

}

namespace __GroupUser;

use __User\User;
use ISmallTable;

class GroupUser implements ISmallTable{

    private $id_group;
    private $name;
    private $date_creation;

    /**
     * GroupUser constructor.
     * @param int $id_group
     * @param string $name
     * @param mixed $date_creation
     */
    public function __construct($id_group, $name, $date_creation)
    {
        $this->id_group = $id_group;
        $this->name = $name;
        $this->date_creation = $date_creation;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return [];
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return GroupUser|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_group_user WHERE id_group_user=:id", array(':id'=>$id), false);

        $result = $request->getResult();

        return new GroupUser($result['id_group_user'], $result['name'],$result['date_creation']);
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;
        $results = [];

        $request = $database->request("SELECT * FROM dbs_group_user", [],true);

        foreach ($request->getResult() as $item){
            array_push($results, new GroupUser($item['id_group_user'], $item['name'], $item['date_creation']));
        }

        return $results;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id_group;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }
}

class UserInGroup {

    private $id_user;
    private $id_group;

    public function __construct($user, $group)
    {
        $this->id_user = $user;
        $this->id_group = $group;
    }

    public static function getAllByGroup($id_group) {

        global $database;

        $returns = [];

        $request = $database->request("SELECT * FROM dbs_be_in WHERE id_group_user=:id", [':id'=>$id_group], true);

        foreach ($request->getResult() as $result){
            array_push($returns, new UserInGroup($result['id_user'], $result['id_group_user']));
        }

        return $returns;
    }

    /**
     * @return mixed
     */
    public function getIdGroup()
    {
        return $this->id_group;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::getById($this->id_user);
    }

}