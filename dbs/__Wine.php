<?php

namespace __Wine;

use ISmallRelation;
use ISmallTable;

class Wine implements ISmallTable {

    /** @var int $id */
    private $id;
    /** @var string $name */
    private $name;
    /** @var string $description */
    private $description;
    /** @var double $degree */
    private $degree;
    /** @var int $id_appellation */
    private $id_appellation;

    private $cepage;
    private $recolte;
    private $vinification;
    private $robe;
    private $nez;
    private $bouche;
    private $accord;
    private $evolution;

    public function __construct($id, $name, $description, $degree, $id_appellation)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->degree = $degree;
        $this->id_appellation = $id_appellation;
    }

    /**
     * Créer un nouveau vin dans la base de données
     * @return bool
     */
    public function create()
    {
        global $database;

        if($this->id == -1) {
            $request = $database->insert('dbs_wine', ['name'=>$this->name, 'description'=>$this->description,'degree'=>$this->degree,'id_appellation'=>$this->id_appellation]);

            return $request->isSuccess();
        }
        return false;
    }

    /**
     * Supprime un vin ainsi que toutes ses relations
     * @return bool
     */
    public function delete()
    {
        global $database;

        $request = $database->delete('dbs_wine', ['id_wine'=>$this->id]);

        return $request->isSuccess();
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Wine|null
     */
    public static function getById($id)
    {
        global $database;

        if($database->isConnected()){

            $wine = null;
            $request = $database->request("SELECT * FROM dbs_wine WHERE id_wine=:id", array(':id'=>$id));
            $request->execute();

            if($request->getCount() > 0 ) {
                $item = $request->getResult();
                $wine = new Wine($item['id_wine'], $item['name'], $item['description'], $item['degree'], $item['id_appellation']);

                $wine->setListCepage(CepageRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListVinification(VinificationRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListEvolution(EvolutionRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListAccord(AccordRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListBouche(BoucheRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListNez(NezRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListRecolte(RecolteRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListRobe(RobeRel::findById(['id_wine' => $item['id_wine']]));

                return $wine;
            } else {
                return null;
            }
        } else {
            die("Impossible de se connecter à la base de données.");
        }
    }


    /**
     * Return an list of the {@class}
     * @param bool $relation Vérifie si il doit retourner les relations liés
     * @return array|null
     */
    public static function getAll($relation = true)
    {
        global $database;
        $result = [];

        $stmt = $database->request("SELECT * FROM dbs_wine", [], true);
        $stmt->execute();

        foreach ($stmt->getResult() as $item){
            $wine = new Wine($item['id_wine'], $item['name'],$item['description'],$item['degree'],$item['id_appellation']);

            if($relation) {
                $wine->setListCepage(CepageRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListVinification(VinificationRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListEvolution(EvolutionRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListAccord(AccordRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListBouche(BoucheRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListNez(NezRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListRecolte(RecolteRel::findById(['id_wine' => $item['id_wine']]));
                $wine->setListRobe(RobeRel::findById(['id_wine' => $item['id_wine']]));
            }

            array_push($result, $wine);
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return is_null($this->description) || $this->description=="" ? "Aucune description" : $this->description;
    }

    /**
     * @return float
     */
    public function getDegree(): float
    {
        return $this->degree;
    }

    /**
     * @return Appellation
     */
    public function getAppellation(): Appellation
    {
        return Appellation::getById($this->id_appellation);
    }

    /**
     * Définis la nouvelle liste de cepage
     * @param array $list
     */
    private function setListCepage($list)
    {
        $this->cepage = $list;
    }

    /**
     * Retourne une liste de la class CepageRel ou une classe selon l'index
     * @param int $index
     * @return array|CepageRel
     */
    public function getListCepage($index = -1)
    {
        return $index < 0 ? $this->cepage : $this->cepage[$index];
    }

    /**
     * Définis la nouvelle liste de vinification
     * @param array $list
     */
    private function setListVinification($list)
    {
        $this->vinification = $list;
    }

    /**
     * Retourne une liste de la class VinificationRel ou une classe selon l'index
     * @param int $index
     * @return array|VinificationRel
     */
    public function getListVinification($index = -1)
    {
        return $index < 0 ? $this->vinification : $this->vinification[$index];
    }

    /**
     * Définis la nouvelle liste de accord
     * @param array $list
     */
    private function setListAccord($list)
    {
        $this->accord = $list;
    }

    /**
     * Retourne une liste de la class AccordRel ou une classe selon l'index
     * @param int $index
     * @return array|AccordRel
     */
    public function getListAccord($index = -1)
    {
        return $index < 0 ? $this->accord : $this->accord[$index];
    }

    /**
     * Définis la nouvelle liste de bouche
     * @param array $list
     */
    private function setListBouche($list)
    {
        $this->bouche = $list;
    }

    /**
     * Retourne une liste de la class CepageRel ou une classe selon l'index
     * @param int $index
     * @return array|BoucheRel
     */
    public function getListBouche($index = -1)
    {
        return $index < 0 ? $this->bouche : $this->bouche[$index];
    }

    /**
     * Définis la nouvelle liste de nez
     * @param array $list
     */
    private function setListNez($list)
    {
        $this->nez = $list;
    }

    /**
     * Retourne une liste de la class NezRel ou une classe selon l'index
     * @param int $index
     * @return array|NezRel
     */
    public function getListNez($index = -1)
    {
        return $index < 0 ? $this->nez : $this->nez[$index];
    }

    /**
     * Définis la nouvelle liste de robe
     * @param array $list
     */
    private function setListRobe($list)
    {
        $this->robe = $list;
    }

    /**
     * Retourne une liste de la class CepageRel ou une classe selon l'index
     * @param int $index
     * @return array|RobeRel
     */
    public function getListRobe($index = -1)
    {
        return $index < 0 ? $this->robe : $this->robe[$index];
    }

    /**
     * Définis la nouvelle liste de recolte
     * @param array $list
     */
    private function setListRecolte($list)
    {
        $this->vinification = $list;
    }

    /**
     * Retourne une liste de la class RecolteRel ou une classe selon l'index
     * @param int $index
     * @return array|RecolteRel
     */
    public function getListRecolte($index = -1)
    {
        return $index < 0 ? $this->recolte : $this->recolte[$index];
    }

    /**
     * Définis la nouvelle liste de evolution
     * @param array $list
     */
    private function setListEvolution($list)
    {
        $this->evolution = $list;
    }

    /**
     * Retourne une liste de la class EvolutionRel ou une classe selon l'index
     * @param int $index
     * @return array|EvolutionRel
     */
    public function getListEvolution($index = -1)
    {
        return $index < 0 ? $this->evolution : $this->evolution[$index];
    }

    /**
     * @return string
     */
    public function getInfos() {
        $message = "<p>IGP : " . $this->getAppellation()->getLibelle() ."</p>";

        $message .= "<p>Cépages : ";
        foreach ($this->getListCepage() as $item) {
            $message .= $item->getCepage()->getLibelle() . " > ". $item->getPercentage();
            $message .= ", ";
        }
        $message .= "</p>";

        $message .= "<p>Accord : ";
        foreach ($this->getListAccord() as $cepage) {
            $message .= $cepage->getAccord()->getLibelle();
            $message .= ", ";
        }
        $message .= "</p>";

        $message .= "<p>Bouche : ";
        foreach ($this->getListCepage() as $cepage) {
            $message .= $cepage->getAccord()->getLibelle();
            $message .= ", ";
        }
        $message .= "</p>";

        return $message;
    }

}

class Appellation implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_appellation'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Appellation|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_appellation WHERE id_appellation=:id", array(':id'=>$id));
        $request->execute();
        $appellation = null;

        if($request->getRowCount() == 1)
            $appellation = new Appellation($request->getResult()['id_appellation'],$request->getResult()['libelle']);

        return $appellation;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_appellation", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Appellation($item['id_appellation'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Cepage implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_cepage'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Cepage|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_cepage WHERE id_cepage=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Cepage($request->getResult()['id_cepage'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_cepage", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Appellation($item['id_cepage'], $item['libelle']));
        }

        return $results;
    }

    public static function getAllByRel($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_cepage 
                  INNER JOIN dbs_cepage_rel 
                  ON dbs_cepage.id_cepage = dbs_cepage_rel.id_cepage
                  WHERE dbs_cepage_rel.id_wine=:id", [':id'=>$id], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Cepage($item['id_cepage'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

}

class Recolte implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_recolte'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Recolte|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_recolte WHERE id_recolte=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Recolte($request->getResult()['id_recolte'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_recolte", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Recolte($item['id_recolte'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Vinification implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_vinification'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Vinification|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_vinification WHERE id_vinification=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Vinification($request->getResult()['id_vinification'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_vinification", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Vinification($item['id_vinification'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Robe implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_robe'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Robe|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_robe WHERE id_robe=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Robe($request->getResult()['id_robe'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_robe", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Robe($item['id_robe'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Nez implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_nez'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Nez|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_nez WHERE id_nez=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Nez($request->getResult()['id_nez'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_nez", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Nez($item['id_nez'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Bouche implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_bouche'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Bouche|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_bouche WHERE id_bouche=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Bouche($request->getResult()['id_bouche'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_bouche", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Bouche($item['id_bouche'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Accord implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_accord'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Accord|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_accord WHERE id_accord=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Accord($request->getResult()['id_accord'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_accord", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Accord($item['id_accord'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Evolution implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        return array('id_evolution'=>$this->id, 'libelle'=>$this->libelle);
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Evolution|null
     */
    public static function getById($id)
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_evolution WHERE id_evolution=:id", array(':id'=>$id));
        $request->execute();
        $res = null;

        if($request->getRowCount() == 1)
            $res = new Evolution($request->getResult()['id_evolution'],$request->getResult()['libelle']);

        return $res;
    }

    /**
     * Return an list of the {@class}
     * @return array|null
     */
    public static function getAll()
    {
        global $database;

        $request = $database->request("SELECT * FROM dbs_evolution", [], true);
        $request->execute();
        $results = [];

        foreach ($request->getResult() as $item){
            array_push($results, new Evolution($item['id_evolution'], $item['libelle']));
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

class Conditionnement implements ISmallTable {

    private $id;
    private $libelle;

    public function __construct($id, $libelle)
    {
        $this->id = $id;
        $this->libelle = $libelle;
    }

    /**
     * Return the {@class} to an array
     * @return array
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

    /**
     * Return the class by the ID
     * @param $id
     * @return Conditionnement|null
     */
    public static function getById($id)
    {
        global $database;
        $request = $database->request("SELECT * FROM dbs_conditionnement WHERE id_conditionnement=:id", [':id'=>$id]);

        $item = $request->getResult();
        if(count($item)>0){
            return new Conditionnement($item['id_conditionnement'], $item['libelle']);
        }

        return null;
    }

    /**
     * Return an list of the Conditionnement
     * @return array|null
     */
    public static function getAll()
    {
        global $database;
        $returns = [];
        $request = $database->request("SELECT * FROM dbs_conditionnement", [], true);

        foreach ($request->getResult() as $item) {
            array_push($returns, new Conditionnement($item['id_conditionnement'], $item['libelle']));
        }

        return $returns;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}

/* RELATION */
abstract class Relation implements ISmallRelation{

    /** @var int $id_own : L'id qui sera le maître */
    protected $id_own;
    /** @var int $id_rel : L'id qui sera la relation */
    protected $id_rel;

    public function __construct($id_own, $id_rel)
    {
        $this->id_own = $id_own;
        $this->id_rel = $id_rel;
    }

    /**
     * @return int
     */
    public function getIdOwn(): int
    {
        return $this->id_own;
    }

    /**
     * @return int
     */
    public function getIdRel(): int
    {
        return $this->id_rel;
    }

}

class CepageRel extends Relation {

    /** @var float $percentage Le pourcentage du cepage */
    private $percentage;

    public function __construct($id_wine, $id_cepage, $percentage)
    {
        parent::__construct($id_wine, $id_cepage);
        $this->percentage = $percentage;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }

        $request = $database->request(
            "SELECT dbs_cepage_rel.id_wine, dbs_cepage_rel.id_cepage, dbs_cepage_rel.percentage 
                FROM dbs_cepage_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new CepageRel($item['id_wine'], $item['id_cepage'], $item['percentage']));
        }

        return $result;
    }

    /**
     * Retourne le cepage
     * @return \__DBS__Class\Cepage|null
     */
    public function getCepage()
    {
        return Cepage::getById($this->getIdRel());
    }

}

class VinificationRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }


        $request = $database->request(
            "SELECT id_wine, id_vinification
                FROM dbs_vinification_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new VinificationRel($item['id_wine'], $item['id_vinification']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Vinification|null
     */
    public function getVinification()
    {
        return Vinification::getById($this->getIdRel());
    }

}

class RobeRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }

        $request = $database->request(
            "SELECT id_wine, id_robe
                FROM dbs_robe_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new RobeRel($item['id_wine'], $item['id_robe']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Robe|null
     */
    public function getRobe()
    {
        return Robe::getById($this->getIdRel());
    }

}

class NezRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }



        $request = $database->request(
            "SELECT id_wine, id_nez
                FROM dbs_nez_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new NezRel($item['id_wine'], $item['id_nez']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Nez|null
     */
    public function getNez()
    {
        return Nez::getById($this->getIdRel());
    }

}

class BoucheRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }



        $request = $database->request(
            "SELECT id_wine, id_bouche
                FROM dbs_bouche_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new BoucheRel($item['id_wine'], $item['id_bouche']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Bouche|null
     */
    public function getBouche()
    {
        return Bouche::getById($this->getIdRel());
    }

}

class RecolteRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }



        $request = $database->request(
            "SELECT id_wine, id_recolte
                FROM dbs_recolte_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new VinificationRel($item['id_wine'], $item['id_recolte']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Recolte|null
     */
    public function getRecolte()
    {
        return Recolte::getById($this->getIdRel());
    }

}

class EvolutionRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }



        $request = $database->request(
            "SELECT id_wine, id_evolution
                FROM dbs_evolution_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new EvolutionRel($item['id_wine'], $item['id_evolution']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Evolution|null
     */
    public function getEvolution()
    {
        return Evolution::getById($this->getIdRel());
    }

}

class AccordRel extends Relation {

    /**
     * Find the relation by an id
     * @param $array
     * @return array
     */
    public static function findById($array)
    {
        global $database;
        $result = [];
        $where = "";

        $i = count($array);
        foreach ($array as $key=>$value){
            $where .= $key . "=" . $value;
            $i--;
            if($i != 0)
                $where .= " AND ";
        }



        $request = $database->request(
            "SELECT id_wine, id_accord
                FROM dbs_accord_rel
                WHERE ".$where, array(),true);
        $request->execute();

        foreach ($request->getResult() as $item){
            array_push($result, new AccordRel($item['id_wine'], $item['id_accord']));
        }

        return $result;
    }

    /**
     * @return \__DBS__Class\Accord|null
     */
    public function getAccord()
    {
        return Accord::getById($this->getIdRel());
    }

}