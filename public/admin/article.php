<?php

use __Item\Item;
use __Item\RegisterItem;
use __Message\Message;
use __Picture\Picture;
use __Wine\Conditionnement;
use __Wine\Wine;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    if(isset($_POST['libelle']) && isset($_POST['price']) && isset($_POST['stock'])
    && isset($_POST['id_wine']) && isset($_POST['id_cond']) && isset($_POST['id_picture'])) {

        $token = generate_token();
        $name = htmlspecialchars($_POST['libelle']);
        $description = htmlspecialchars($_POST['description']);
        $details = htmlspecialchars($_POST['details']);
        $price = doubleval($_POST['price']);
        $promotion = empty($_POST['promotion']) ? 0 : floatval($_POST['promotion']);
        $stock = intval($_POST['stock']);
        $id_wine = intval($_POST['id_wine']);
        $id_cond = intval($_POST['id_cond']);
        $id_picture = intval($_POST['id_picture']);
        $date = date('Y-m-d');

        $item = new RegisterItem($token, $name, $description, $details, $price, $promotion,
            $stock, $id_wine, $id_cond, $id_picture, $date);

        $message = Message::findById('article', ($item->create() ? 1 : 2));

    }
}

//Get for the delete row
if($_GET) {

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="block big-block">
        <div class="block-header">
            <h2>Ajouter un article</h2>
        </div>
        <div class="block-content">
            <form method="post">
                <div class="form-group">
                    <div class="form-part">
                        <label>
                            <input type="text" name="libelle" placeholder="Nom de l'article" required>
                        </label>
                        <label>
                            <input type="text" name="description" placeholder="Courte description de l'article" minlength="15" maxlength="255" required>
                        </label>
                    </div>
                    <div class="form-part">
                        <label>
                            <span>Plus en détails :</span>
                            <textarea name="details" cols="30" rows="10" required></textarea>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-part">
                        <label>
                            <input name="price" type="number" step="0.01" min="0" max="5000" placeholder="Prix à l'unité" required>
                        </label>
                        <label>
                            <input name="promotion" type="number" step="0.0001" min="0" max="1" placeholder="Promotion de l'article">
                        </label>
                        <label>
                            <input name="stock" type="number" step="1" min="0" max="500000" placeholder="Quantité disponible" required>
                        </label>
                    </div>
                    <!-- Vin Conditionnement Picture -->
                    <div class="form-part">
                        <label for="">
                            <select name="id_wine" required>
                                <option value="" hidden selected disabled>Choisissez un vin</option>
                                <?php
                                $objs = Wine::getAll(false);

                                /** @var Wine $obj */
                                foreach ($objs as $obj) {
                                    echo "<option value='".$obj->getId()."'>".$obj->getName()."</option>";
                                }
                                ?>
                            </select>
                        </label>
                        <label for="">
                            <select name="id_cond" required>
                                <option value="" hidden selected disabled>Choisissez un conditionnement</option>
                                <?php
                                $objs = Conditionnement::getAll();

                                /** @var Conditionnement $obj */
                                foreach ($objs as $obj) {
                                    echo "<option value='".$obj->getId()."'>".$obj->getLibelle()."</option>";
                                }
                                ?>
                            </select>
                        </label>
                        <label for="">
                            <select name="id_picture" required>
                                <option value="" hidden selected disabled>Choisissez une image de profil</option>
                                <?php
                                $objs = Picture::getAll();

                                /** @var Picture $obj */
                                foreach ($objs as $cat=>$item) {
                                    echo "<optgroup label='$cat'></optgroup>";
                                    foreach ($item as $obj){
                                        echo "<option value='".$obj->getId()."'>".$obj->getName()."</option>";
                                    }
                                 }
                                ?>
                            </select>
                        </label>
                    </div>
                </div>

                <button class="btn" type="submit">Créer</button>

            </form>
        </div>
    </div>

    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Articles</h2>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th style="width: 100px;">N* Article</th>
                    <th>Nom</th>
                    <th>Prix/U</th>
                    <th>Promotion</th>
                    <th>En Stock</th>
                    <th>Vin</th>
                    <th>Conditionnement</th>
                    <th>Litre</th>
                    <th>Visible</th>
                    <th>Disponible</th>
                    <th>Action</th>
                </tr>
                <?php

                $articles = Item::getAll();

                /** @var Item $article */
                foreach ($articles as $article) {
                    echo "<tr>";

                    $id = $article->getId();
                    echo "<td>$id</td>";
                    echo "<td>".$article->getLibelle()."</td>";
                    echo "<td>".$article->getPrice()."€/unité</td>";
                    echo "<td>".round($article->getPromotion()*100)."%</td>";
                    echo "<td>".$article->getStock()."</td>";
                    echo "<td>".$article->getWine()->getName()."</td>";
                    echo "<td>".$article->getConditionnement()->getLibelle()."</td>";
                    echo "<td>".$article->getLitre()."</td>";
                    echo "<td>".($article->isShow() ? "Oui" : "Non") ."</td>";
                    echo "<td>".$article->getDisponible()->getLibelle()."</td>";
                    echo "<td><a class='afa fa-blue' href='article_edit.php?id_item=$id'><i class='fas fa-edit'></i></a> </td>";

                    echo "</tr>";
                }

                ?>
            </table>
        </div>
    </div>

</main>
</body>
</html>
