<?php

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    if(isset($_POST['libelle']) && isset($_POST['price']) && isset($_POST['stock'])
    && isset($_POST['id_wine']) && isset($_POST['id_cond']) && isset($_POST['id_picture'])) {

        $token = generate_token();
        $name = htmlspecialchars($_POST['libelle']);
        $description = htmlspecialchars($_POST['description']);
        $details = htmlspecialchars($_POST['details']);
        $price = doubleval($_POST['price']);
        $promotion = empty($_POST['promotion']) ? 0 : floatval($_POST['promotion']);
        $stock = intval($_POST['stock']);
        $id_wine = intval($_POST['id_wine']);
        $id_cond = intval($_POST['id_cond']);
        $id_picture = intval($_POST['id_picture']);
        $date = date('Y-m-d');

        $item = new RegisterItem($token, $name, $description, $details, $price, $promotion,
            $stock, $id_wine, $id_cond, $id_picture, $date);

        $message = Message::findById('article', ($item->create() ? 1 : 2));

    }
}

if($_GET) {

    if(isset($_GET['id_item'])) {
        $article = Item::getById($_GET['id_item']);
    } else {
        redirect('article.php');
    }

}

use __DBS__Class\Disponible;
use __Item\Item;
use __Item\RegisterItem;
use __Message\Message;
use __Picture\Picture;

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="block big-block">
        <div class="block-header">
            <h3>Modifier les champs de l'article : <?php echo $article->getLibelle(); ?></h3>
        </div>
        <div class="block-content">
            <form method="post">
                <div class="form-group">
                    <div class="form-part">
                        <label>
                            <input type="text" name="libelle" placeholder="Nom de l'article" value="<?= $article->getLibelle(); ?>">
                        </label>
                        <label>
                            <input type="text" name="description" placeholder="Courte description de l'article" minlength="15" maxlength="255" value="<?= $article->getDescription();?>">
                        </label>
                    </div>
                    <div class="form-part">
                        <label>
                            <span>Plus en détails :</span>
                            <textarea name="details" cols="30" rows="10"><?= $article->getDetails();?></textarea>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-part">
                        <label>
                            <span>Prix en € :</span>
                            <input name="price" type="number" step="0.01" min="0" max="5000" placeholder="Prix à l'unité" value="<?= $article->getPrice();?>">
                        </label>
                        <label>
                            <span>Promotion : </span>
                            <input name="promotion" type="number" step="0.0001" min="0" max="1" placeholder="Promotion de l'article" value="<?= $article->getPromotion();?>">
                        </label>
                        <label>
                            <span>En stock :</span>
                            <input name="stock" type="number" step="1" min="0" max="500000" placeholder="Quantité disponible" value="<?= $article->getStock();?>">
                        </label>
                    </div>
                    <!-- Vin Conditionnement Picture -->
                    <div class="form-part">
                        <label for="">
                            <select name="id_picture" required>
                                <?php
                                $picture = $article->getPicture();
                                $objs = Picture::getAll();

                                /** @var Picture $obj */
                                foreach ($objs as $cat=>$item) {
                                    echo "<optgroup label='$cat'></optgroup>";
                                    foreach ($item as $obj){
                                        if($picture->getId() == $obj->getId())
                                            echo "<option value='".$obj->getId()."' selected>".$obj->getName()."</option>";
                                        else
                                            echo "<option value='".$obj->getId()."'>".$obj->getName()."</option>";
                                    }
                                }
                                ?>
                            </select>
                            <span>Image actuelle :</span>
                            <img style="margin-top: 10px; width: 150px; height: auto;" src="<?= $picture->getUrl(); ?>" alt="">
                        </label>
                    </div>
                </div>

                <button class="btn" type="submit">Mettre à jour</button>

            </form>
        </div>
    </div>

    <div class="row-block">
        <div class="block">
            <div class="block-header">
                <h3>Changer la visibilité de l'article</h3>
            </div>
            <div class="block-content">
                <ul>
                    <li>La visibilité de l'article n'influe pas sur sa disponibilité.</li>
                    <li>Elle permet seulement d'afficher ou non l'article sur le site.</li>
                </ul>
                <p>Visibilité actuel : <b><?= ($article->isShow()?"Visible": "Caché");?></b></p>
                <a href="article_edit.php?type=visibility&id_item=<?= $article->getId();?>" class="btn"><?= (!$article->isShow()?"Afficher": "Cacher");?></a>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Changer la disponibilité de l'article</h3>
            </div>
            <div class="block-content">
                <ul>
                    <li>La disponibilité permet d'indiquer au client si l'article est disponible ou non à l'achat.</li>
                </ul>
                <p>Disponibilité actuel : <b><?= $article->getDisponible()->getLibelle();?></b></p>
                <form method="get">
                    <label for="" hidden>
                        <input type="hidden" name="type" value="disponible">
                    </label>
                    <label for="">
                        <select name="disponible" >
                            <?php
                            $dispos = Disponible::getAll();
                            /** @var Disponible $dispo */
                            foreach ($dispos as $dispo){
                                echo "<option value='".$dispo->getId()."'>".$dispo->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Modifier</button>
                </form>
            </div>
        </div>
    </div>

</main>
</body>
</html>
