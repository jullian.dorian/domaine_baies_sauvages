<?php

use __Message\Message;
use __Wine\Accord;
use __Wine\Appellation;
use __Wine\Bouche;
use __Wine\Cepage;
use __Wine\Conditionnement;
use __Wine\Evolution;
use __Wine\Nez;
use __Wine\Recolte;
use __Wine\Robe;
use __Wine\Vinification;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    $post = isset($_POST['name_appellation']) ? ['igp', htmlspecialchars($_POST['name_appellation'])] : (
        isset($_POST['name_cepage']) ? ['cepage', htmlspecialchars($_POST['name_cepage'])] : (
            isset($_POST['name_rec']) ? ['recolte', htmlspecialchars($_POST['name_rec'])] : (
                isset($_POST['name_vini']) ? ['vinification', htmlspecialchars($_POST['name_vini'])] :(
                    isset($_POST['name_robe']) ? ['robe', htmlspecialchars($_POST['name_robe'])] : (
                        isset($_POST['name_nez']) ? ['nez', htmlspecialchars($_POST['name_nez'])] : (
                            isset($_POST['name_bouche']) ? ['bouche', htmlspecialchars($_POST['name_bouche'])] : (
                                isset($_POST['name_accord']) ? ['accord', htmlspecialchars($_POST['name_accord'])] : (
                                    isset($_POST['name_evo']) ? ['evolution', htmlspecialchars($_POST['name_evo'])] : (
                                        isset($_POST['name_cond']) ? ['conditionnement', htmlspecialchars($_POST['name_cond'])] : null
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    );

    $type = $post[0];
    $name = $post[1];

    $table = "dbs_".$type;
    $args = ['libelle' => $name];

    $request = $database->insert($table, $args);

    if($request->isSuccess()) {
        $message = Message::findById("categorie", 1);
    } else {
        $message = Message::findById("categorie", 2);
    }

}

//Get for the delete row
if($_GET) {
    if(isset($_GET['id']) && isset($_GET['table'])) {

        $id = $_GET['id'];
        $table = "dbs_" . $_GET['table'];

        $request = $database->delete($table, ['id_'.$_GET['table'] => $id]);

        if($request->isSuccess()) {
            $message = Message::findById("categorie", 3);
        } else {
            $message = Message::findById("categorie", 4);
        }

    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>
    <!-- Conditionnement -->
    <div class="row-block">
        <div class="block block-red block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter un Conditionnement</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_cond">
                        <input type="text" name="name_cond" placeholder="Nom du Conditionnement" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block block-red">
            <div class="block-header">
                <h2>Liste/Action des Conditionnements</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Cond</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Conditionnement::getAll();

                    /** @var Conditionnement $cat */
                    foreach ($category as $cat){
                        echo "<tr>";

                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=conditionnement' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Appellation -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Appellation</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_appellation">
                        <input type="text" name="name_appellation" placeholder="Nom de l'IGP" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des IGP</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Ap.tion</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Appellation::getAll();

                    /** @var Appellation $cat */
                    foreach ($category as $cat){
                        echo "<tr>";

                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=igp' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Cepage -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter un Cépage</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_cepage">
                        <input type="text" name="name_cepage" placeholder="Nom du cépage" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Cépages</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Cépage</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Cepage::getAll();

                    /** @var Cepage $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=cepage' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Recolte -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Récolte</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_rec">
                        <input type="text" name="name_rec" placeholder="Nom de la récolte" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Récoltes</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Recolte</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Recolte::getAll();

                    /** @var Recolte $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=recolte' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Vinification -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Vinification</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_vini">
                        <input type="text" name="name_vini" placeholder="Nom de la vinification" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Vinifications</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Vinification</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Vinification::getAll();

                    /** @var Vinification $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=vinification' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Robe -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Robe</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_robe">
                        <input type="text" name="name_robe" placeholder="Nom de la robe" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Robes</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Robe</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Robe::getAll();

                    /** @var Robe $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=robe' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Nez -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter un Nez</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_nez">
                        <input type="text" name="name_nez" placeholder="Nom du nez" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Nez</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Nez</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Nez::getAll();

                    /** @var Nez $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=nez' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Bouche -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Bouche</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_bouche">
                        <input type="text" name="name_bouche" placeholder="Nom de la bouche" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Bouches</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Bouche</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Bouche::getAll();

                    /** @var Bouche $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=bouche' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Accords -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter un Accord</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_accord">
                        <input type="text" name="name_accord" placeholder="Nom de l'accord" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Accords</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Accord</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Accord::getAll();

                    /** @var Accord $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=accord' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Evolution -->
    <div class="row-block">
        <div class="block block-sm align-self-start">
            <div class="block-header">
                <h2>Ajouter une Evolution</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="name_evo">
                        <input type="text" name="name_evo" placeholder="Nom de l'évolution" required>
                    </label>
                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Liste/Action des Evolutions</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Evolution</th>
                        <th>Libellé</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $category = Evolution::getAll();

                    /** @var Evolution $cat */
                    foreach ($category as $cat){
                        echo "<tr>";
                        $id = $cat->getId();
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $cat->getLibelle() . "</td>";
                        echo "<td><a class='afa afa-align fa-red' href='category.php?id=$id&table=evolution' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a></td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>
