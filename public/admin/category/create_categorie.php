<?php

require '../../../dbs/__DBS.php';

if($_POST){

    $table = htmlspecialchars($_POST['table']);
    $name = htmlspecialchars($_POST['name']);

    $request = $database->insert($table, ['libelle' => $name]);

    if($request->isSuccess()){
        $database->stop();

        header('location: index.php');
        die();
    } else {
        echo "prob insert";
    }

} else {
    echo "aucun post";
}

?>