<?php

require '../../../dbs/__DBS.php';

if(need_user() || !is_admin($user)){
    redirect('../../index.php');
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="create_categorie.php" method="post">
    <h2>Ajouter un IGP</h2>
    <input type="text" name="table" value="dbs_igp" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de l'IGP</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter une évolution</h2>
    <input type="text" name="table" value="dbs_evolution" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de l'évolution</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter un nez</h2>
    <input type="text" name="table" value="dbs_nez" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom du nez</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter une robe</h2>
    <input type="text" name="table" value="dbs_robe" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de la robe</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter une récolte</h2>
    <input type="text" name="table" value="dbs_recolte" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de la récolte</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter une bouche</h2>
    <input type="text" name="table" value="dbs_bouche" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de la bouche</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter un accord</h2>
    <input type="text" name="table" value="dbs_accord" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de l'accord</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter un cépage</h2>
    <input type="text" name="table" value="dbs_cepage" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom du cépage</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

<form action="create_categorie.php" method="post">
    <h2>Ajouter une vinification</h2>
    <input type="text" name="table" value="dbs_vinification" style="display: none; visibility: hidden;">
    <label for="">
        <span>Nom de la vinification</span>
        <input type="text" name="name">
    </label>
    <button type="submit">Créer</button>
</form>

</body>
</html>