<?php

use __Message\Message;
use __Picture\Diaporama;
use __Picture\Picture;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

/*
 *
Actions :
Add: mettre une image
Visibilite : changer la visibilité
Reset/Supprimer : Remet par defaut l'emplacement

*/

if($_GET) {

    if(isset($_GET['type']) && isset($_GET['id'])) {

        if(ctype_digit(strval($_GET['id']))) {
            $diaporama = Diaporama::getById($_GET['id']);
            switch ($_GET['type']) {
                case "reset":
                    $message = Message::findById('diaporama', ($diaporama->reset() ? 1 :2));
                    break;
                case "add":

                    if(isset($_GET['id_picture'])) {

                        $picture = Picture::getById($_GET['id_picture']);

                        if($picture != null) {
                            $diaporama->setPicture($picture->getId());

                            if($diaporama->updatePicture()) {
                                redirect('diaporama.php');
                                die();
                            }
                        }
                        redirect('diaporama.php&type=add&id='.$_GET['id']);

                    }
                    $add_picture = true;

                    break;
                case "visibility":
                    $message = Message::findById('diaporama',($diaporama->changeVisibility() ? 5 : 6));
                    break;
            }
        }

    }

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }

    if(isset($add_picture) && $add_picture == true) {

        ?>

        <div class="block">
            <div class="block-header">
                <h2>Choisissez l'image à appliquer</h2>
            </div>
            <div class="block-content">
                <form method="get">
                    <p><b>Numéro emplacement : <?php echo $_GET['id']; ?></b></p>
                    <input type="hidden" hidden name="type" value="add">
                    <input type="hidden" hidden name="id" value="<?php echo $_GET['id']; ?>">
                    <label>
                        <select name="id_picture" required>
                            <option value="" hidden selected disabled>Choisissez une image</option>
                            <?php

                            $pictures = Picture::getAll();

                            foreach ($pictures as $cat=>$item) {
                                $cat = ucfirst($cat);
                                echo "<optgroup label='$cat'>";
                                foreach ($item as $picture) {
                                    echo "<option value='".$picture->getId()."'>".$picture->getName()."</option>";
                                }
                                echo "</optgroup>";
                            }

                            ?>
                        </select>
                    </label>
                    <button class="btn">Confirmer</button>
                </form>
            </div>
        </div>

        <?php

    } else {

        ?>

        <div class="block big-block">
            <div class="block-header">
                <h2>Emplacements des images du diaporama</h2>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width: 100px;">N* Emplacement</th>
                        <th>Nom Image</th>
                        <th>Prévisualisation</th>
                        <th>Visible</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    <?php

                    $diapos = Diaporama::getAll();

                    /** @var Diaporama $diapo */
                    foreach ($diapos as $diapo) {
                        echo "<tr>";

                        $picture = $diapo->getPicture();
                        if ($picture == null)
                            $picture = new Picture($diapo->getId(), "Aucune image chargée", "null", "", null);

                        echo "<td>" . $diapo->getId() . "</td>";
                        echo "<td>" . $picture->getName() . "</td>";
                        echo "<td style='text-align: center;'><img style='width: 45px; height: 45px;' src='" . $picture->getUrl() . "'></td>";
                        echo "<td>" . ($diapo->isShow() ? "Oui" : "Non") . "</td>";
                        echo "<td class='td-flex'>" .
                            "<a class='afa fa-blue' href='diaporama.php?type=reset&id=" . $diapo->getId() . "'><i class='fas fa-undo'></i></a>" .
                            "<a class='afa fa-red' href='diaporama.php?type=add&id=" . $diapo->getId() . "'><i class='fas fa-image'></i></a>";
                        if (!$diapo->isShow())
                            echo "<a class='afa fa-orange' href='diaporama.php?type=visibility&id=" . $diapo->getId() . "'><i class='fas fa-eye'></i></a>";
                        else
                            echo "<a class='afa fa-red' href='diaporama.php?type=visibility&id=" . $diapo->getId() . "'><i class='fas fa-eye-slash'></i></a>";

                        echo "</td>";

                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
        <?php
    }
    ?>
</main>
</body>
</html>
