<?php

use __GroupUser\GroupUser;
use __Message\Message;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    if(isset($_POST['name'])) {

        $name = htmlspecialchars($_POST['name']);

        $request = $database->insert("dbs_group_user", ['name'=>$name,'date_creation'=>date('Y-m-d')]);

        $message = Message::findById('group', ($request->isSuccess() ? 1 : 2));
        unset($_POST['name']);
    }

}

//Get for the delete row
if($_GET) {

    if(isset($_GET['type'])) {

        switch ($_GET['type']){
            case "delete":
                if(isset($_GET['id'])) {
                    $id = intval($_GET['id']);

                    $request = $database->delete("dbs_group_user", ['id_group_user'=>$id]);

                    $message = Message::findById('group', ($request->isSuccess() ? 1 : 2));
                }
                break;
        }

    }

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="row-block justify-content-center">
        <div class="block block-md">
            <div class="block-header">
                <h2>Créer un Groupe</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="">
                        <input type="text" name="name" placeholder="Nom du groupe">
                    </label>
                    <button class="btn">Créer</button>
                </form>
            </div>
        </div>
    </div>


    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Groupes</h2>
        </div>
            <table>
                <tr>
                    <th>N* Groupe</th>
                    <th>Nom</th>
                    <th>Crée le</th>
                    <th>Action</th>
                </tr>
                <?php
                $groups = GroupUser::getAll();
                /** @var GroupUser $group */
                foreach ($groups as $group) {
                    echo "<tr>";

                    $id = $group->getId();
                    echo "<td>$id</td>";
                    echo "<td>".$group->getName()."</td>";
                    echo "<td>".$group->getDateCreation()."</td>";
                    echo "<td class='td-flex'>";
                    echo "<a class='afa fa-blue' href='group_list.php?id=$id' title='Ajouter un membre'><i class='fas fa-user-plus'></i></a>";
                    echo "<a class='afa fa-red' href='group.php?type=delete&id=$id' title='Supprimer un groupe'><i class='fas fa-trash'></i></a>";
                    echo "</td>";

                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>

</main>
</body>
</html>
