<?php

use __GroupUser\UserInGroup;
use __User\User;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    if(isset($_POST['type'])) {

        if(isset($_POST['user'])) {

            $id_us = intval($_POST['user']);

            $database->insert("dbs_be_in", ['id_user'=>$id_us, 'id_group_user'=>$_GET['id']]);
            redirect('group_list.php?id='.$_GET['id']);

        }

    }

}

//Get for the delete row
if($_GET) {

    if(isset($_GET['type'])) {

        switch ($_GET['type']){
            case "delete":

                if(isset($_GET['id_user'])) {
                    $id = intval($_GET['id_user']);

                    $database->delete("dbs_be_in", ['id_user'=>$id,'id_group_user'=>$_GET['id']]);

                }
                break;
        }

    }

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="row-block justify-content-center">
        <div class="block block-md">
            <div class="block-header">
                <h2>Ajouter un Utilisateur au groupe</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="">
                        <label for="" hidden>
                            <input type="hidden" name="type" value="add">
                        </label>
                        <select name="user" required>
                            <option value="" hidden selected>Choisissez un utilisateur</option>
                            <?php
                            $users = User::getAll();

                            /** @var User $us */
                            foreach ($users as $us) {
                                if(!$us->isAdmin())
                                    echo "<option value='".$us->getId()."'>". $us->getName() . " " . $us->getSurname() . "</option>";
                            }

                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
    </div>


    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Utilisateurs dans le groupe</h2>
        </div>
            <table>
                <tr>
                    <th>N* Utilisateur</th>
                    <th>Nom & Prenom</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <?php
                $users_in_group = UserInGroup::getAllByGroup($_GET['id']);

                /** @var UserInGroup $user_group */
                foreach ($users_in_group as $user_group) {
                    echo "<tr>";

                    $user = $user_group->getUser();
                    $id = $user->getId();
                    echo "<td>$id</td>";
                    echo "<td>".$user->getName() . " " . $user->getSurname() ."</td>";
                    echo "<td>".$user->getEmail()."</td>";
                    echo "<td class='td-flex'>";
                    echo "<a class='afa fa-red' href='group_list.php?type=delete&id=".$_GET['id']."&id_user=$id' title='Supprimer un groupe'><i class='fas fa-trash'></i></a>";
                    echo "</td>";

                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>

</main>
</body>
</html>
