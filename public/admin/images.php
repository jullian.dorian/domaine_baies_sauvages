<?php

use __Message\Message;
use __Picture\Picture;
use __Picture\RegisterPicture;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST){
    $n = htmlspecialchars($_POST['name_dir']);
    $files = $_FILES['files'];
    $count = count($files['name']);

    $finfo = new finfo(FILEINFO_MIME_TYPE);

    $dir = "../assets/upload/";
    if(!file_exists($dir . $n))
        mkdir($dir . $n);

    $success = false;

    for($i = 0; $i < $count; $i++){

        $tmp = $files['tmp_name'][$i];

        if(move_uploaded_file($tmp,$dir.$n."/".$files['name'][$i])){
            $name = $files['name'][$i];
            $picture = new RegisterPicture($name, $n, $_SERVER['HTTP_REFERER']."assets/upload/$n/$name", date('Y-m-d'));

            $success = $picture->create();

        } else {
            $success = false;
        }
    }

    $message = Message::findById('image', ($success == true ? 1 : 2));
}

if($_GET) {
    if(isset($_GET['type']) && isset($_GET['id'])) {

        if($_GET['type'] == "delete") {

            $id = $_GET['id'];

            $picture = Picture::getById($id);
            if($picture != null && $picture->getId() != null) {
                $message = Message::findById('image', ($picture->delete() ? 3 : 4));
            }
        }

    }
}


?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="row-block">
        <div class="block">
            <div class="block-header">
                <h2>Comment ajouter une image</h2>
            </div>
            <div class="block-content">
                <ul class="item-number">
                    <li>Entrez un nom de répertoire.</li>
                    <li>Sélectionnez les images via le bouton "Select.Fichiers".</li>
                    <li>Attendre que les images soient chargés.</li>
                    <li>Cliquez sur le bouton "Envoyer"</li>
                </ul>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h2>Ajouter une ou plusieurs image</h2>
            </div>
            <div class="block-content">
                <form method="post" enctype="multipart/form-data">
                    <label for="name_dir">
                        <input type="text" name="name_dir" placeholder="Nom du répertoire" required>
                    </label>
                    <input type="file" name="files[]" multiple>
                    <button class="btn" type="submit">Envoyer</button>
                </form>
            </div>
        </div>
    </div>

    <div class="block big-block">
        <div class="block-header">
            <h2>Liste des images par dossier</h2>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th>URL</th>
                    <th>Miniature</th>
                    <th>Action</th>
                </tr>

                <?php

                $pictures = Picture::getAll();

                foreach ($pictures as $categorie=>$list) {
                    echo "<tr style='background-color: #98292d'>
                        <td colspan='3' style='color:white;text-align: center; font-size: 14pt;'>Dossier : $categorie</td></tr>";

                    /** @var Picture $picture */
                    foreach ($list as $picture) {
                        echo "<tr>";

                        echo "<td>".$picture->getName()."</td>";
                        echo "<td style='text-align: center;'><img style='width: 45px; height: 45px;' src='".$picture->getUrl()."'></td>";
                        echo "<td class='td-flex'>".
                        "<a class='afa fa-blue' title='Cliquer pour prévisualiser.' href='".$picture->getUrl()."' target='_blank'><i class='fas fa-eye'></i></a>".
                        "<a class='afa fa-red' title='Cliquer pour supprimer.' href='images.php?type=delete&id=".$picture->getId()."'><i class='fas fa-trash'></i></a>"
                        ."</td>";

                        echo "</tr>";
                    }
                }

                ?>
            </table>
        </div>
    </div>


</main>
</body>
</html>
<!--
                $root = "../../assets/upload/";
                $pattern = "{jpg, jpeg, gif, png}";
                $dirs = scandir($root);

                $files = [];
                $files['root'] = glob($root . "*.$pattern", GLOB_BRACE);
                if(sizeof($files['root']) == 0)
                    unset($files['root']);

                foreach ($dirs as $dir_file) {
                    if($dir_file != "." && $dir_file != "..") {

                        $df = $root . $dir_file;

                        if(is_dir($df)) {
                            $fls = $df . "/*.$pattern";
                            $f = glob($fls, GLOB_BRACE);

                            $files[$dir_file] = $f;
                        }

                    }
                }


                foreach ($files as $cat=>$file) {

                    echo "<tr style='background-color: #98292d'><td colspan='3' style='color:white;text-align: center; font-size: 14pt;'>$cat</td></tr>";

                    foreach ($file as $f) {
                        echo "<tr>";

                        echo "<td>".strval($f)."</td>";
                        echo "<td style='text-align: center;'><img style='width: 30px; height: 45px;' src='$f'></td>";
                        echo "<td></td>";

                        echo "</tr>";
                    }
                }
-->
