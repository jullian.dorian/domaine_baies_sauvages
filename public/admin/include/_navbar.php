<header>
    <div class="head">
        <h1>Administration</h1>
    </div>
    <div class="nav">
        <ul>
            <li class="nav-category">Général</li>
            <li><a href="/admin/">En Général</a></li>
            <li><a href="/admin/diaporama.php">Le Diaporama</a></li>
            <li><a href="/admin/images.php">Les Images</a></li>
            <li class="nav-category">Utilisateurs</li>
            <li><a href="/admin/users.php">Les Utilisateurs</a></li>
            <li><a href="/admin/group.php">Les Groupes</a></li>
            <li class="nav-category">Magasin</li>
            <li><a href="/admin/article.php">Les Articles</a></li>
            <li><a href="/admin/order.php">Les Commandes</a></li>
            <li class="nav-category">Vin</li>
            <li><a href="/admin/wine.php">Les Vins</a></li>
            <li><a href="/admin/category.php">Les Catégories</a></li>
            <li class="empty">&nbsp;</li>
            <li><a href="/">Revenir au site</a></li>
            <li class="empty">&nbsp;</li>
            <li><a style="color: #ffa7a9;" href="/profil/logout.php">Se deconnecter</a></li>
        </ul>
    </div>
</header>