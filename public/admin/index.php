<?php

require '../../dbs/__DBS.php';

if(!need_user() || !$user->isAdmin()){
    redirect('../index.php');
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
</head>
<body id="admin">
    <?php require 'include/_navbar.php'; ?>
    <main>
        <div class="block">
            <div class="block-header">
                <h2>Les derniers articles achetés</h2>
            </div>
            <div class="block-content">
                //Dernier article -> table -> list article
            </div>
        </div>

        <div class="block block-md">
            <div class="block-header">
                <h2>Dernier client inscrit</h2>
            </div>
            <div class="block-content">
                <?php

                $req = $database->request("SELECT * FROM dbs_user ORDER BY date_creation DESC LIMIT 1");

                $last_user = \__User\User::fromArray($req->getResult());

                echo "<p><b>".$last_user->getName() . "</b> " . $last_user->getSurname() . " | " . $last_user->getAge() . " ans." ."</p>";
                echo "<p>".$last_user->getEmail() . " | " . $last_user->getTelephone() . " | " . $last_user->getAddress() ."</p>";
                echo "<p>Déjà client : " . ($last_user->isClient() ? "Oui" : "Non") . "</p>";
                echo "<p>Inscrit le : <b>". $last_user->getDateCreation() ."</b></p>";
                ?>
            </div>
        </div>
        <div class="row-block">
            <div class="block">
                <div class="block-header">
                    <h2>Nombre de visite aujourd'hui</h2>
                </div>
                <div class="block-content">
                    <?php
                    $nb_visite_today = Visite::getVisiteToday();

                    echo "<p>Il y'a eu : <b>$nb_visite_today</b> visite aujourd'hui.</p>";
                    ?>
                </div>
            </div>
            <div class="block">
                <div class="block-header">
                    <h2>Nombre de visite dans le mois</h2>
                </div>
                <div class="block-content">
                    <?php
                    $nb_visite_month = Visite::getVisiteMonth();

                    echo "<p>Il y'a eu : <b>$nb_visite_month</b> visite dans le mois.</p>";
                    ?>
                </div>
            </div>
            <div class="block">
                <div class="block-header">
                    <h2>Nombre de visite dans l'année</h2>
                </div>
                <div class="block-content">
                    <?php
                    $nb_visite_year = Visite::getVisiteYear();

                    echo "<p>Il y'a eu : <b>$nb_visite_year</b> visite dans l'année.</p>";
                    ?>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
