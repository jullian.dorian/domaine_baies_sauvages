<?php

use __Order\Order;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {


}

//Get for the delete row
if($_GET) {

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Commandes</h2>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th style="width: 100px;">N* Order</th>
                    <th>Nom/Prenom</th>
                    <th>Crée le</th>
                    <th>Action</th>
                </tr>
                <?php

                $orders = Order::getAll();

                /** @var Order $order */
                foreach ($orders as $order) {
                    echo "<tr>";

                    $id = $order->getId();
                    echo "<td>$id</td>";
                    echo "<td>". $order->getUser()->getName()."</td>";
                    echo "<td>".$order->getDateCreation()."</td>";
                    echo "<td></td>";

                    echo "</tr>";
                }

                ?>
            </table>
        </div>
    </div>

</main>
</body>
</html>
