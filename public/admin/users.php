<?php

use __Message\Message;
use __User\User;

require '../../dbs/__DBS.php';

if(!need_user() || !$user->isAdmin()){
    redirect('../index.php');
}

//Get for the delete row
if($_GET) {
    if(isset($_GET['id']) && isset($_GET['to']) && isset($_GET['type'])) {

        if($_GET['type'] == "change") {

            $_user = User::getById($_GET['id']);

            if($_user->changeTo($_GET['to'])){
                $message = Message::findById('user', 1);
            } else {
                $message = Message::findById('user', 2);
            }
        }
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>
    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Utilisateurs</h2>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th style="width: 50px;">N* User</th>
                    <th>Nom / Prenom</th>
                    <th>Âge</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Adresse</th>
                    <th>Déjà client</th>
                    <th>Admin</th>
                    <th>Crée le</th>
                    <th style="width: 100px;">Action</th>
                </tr>
                <?php

                $users = User::getAll();

                /** @var User $us */
                foreach ($users as $us) {
                    echo "<tr>";

                    $id = $us->getId();
                    echo "<td>" . $id . "</td>";
                    echo "<td>" . $us->getName() . " " . $us->getSurname() . "</td>";
                    echo "<td>" . $us->getAge() . "</td>";
                    echo "<td>" . $us->getEmail() . "</td>";
                    echo "<td>" . $us->getTelephone() . "</td>";
                    echo "<td>" . $us->getAddress() . "</td>";
                    echo "<td>" . ($us->isClient() ? "Oui" : "Non") . "</td>";
                    echo "<td>" . ($us->isAdmin() ? "Oui" : "Non") . "</td>";
                    echo "<td>" . $us->getDateCreation(). "</td>";
                    echo "<td class='td-flex'>";
                        echo "<a class='afa fa-blue' href='mailto:".$us->getEmail()."' target='_blank' title='Mailer cette ligne'><i class='fas fa-envelope'></i></a>";
                        if($us->isAdmin() && $us->getId() != 1) {
                            echo "<a class='afa fa-blue' href='users.php?type=change&id=$id&to=user' title='Changer en utilisateur'><i class='fas fa-user'></i></a>";
                        } else if($us->getId() != 1){
                            echo "<a class='afa fa-red' href='users.php?type=change&id=$id&to=admin' title='Changer en administrateur'><i class='fas fa-user-tie'></i></a>";
                        }
                    echo "</td>";

                    echo "</tr>";
                }

                ?>
            </table>
        </div>
    </div>
</main>
</body>
</html>
