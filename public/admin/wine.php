<?php

use __Message\Message;
use __Wine\Appellation;
use __Wine\Wine;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    $name = htmlspecialchars($_POST['name_wine']);
    $description = isset($_POST['description_wine']) ? htmlspecialchars($_POST['description_wine']) : null;
    $degree = $_POST['degree_igp'];
    $id_igp = $_POST['id_igp'];

    if($id_igp == -1) {
        $message = Message::findById('wine', 5);
    } else {
        $wine = new Wine(-1, $name, $description, $degree, $id_igp);

        if($wine->create()) {
            $message = Message::findById('wine', 1);
        } else {
            $message = Message::findById('wine', 2);
        }
    }
}

//Get for the delete row
if($_GET) {
    if(isset($_GET['id']) && isset($_GET['type'])) {
        if($_GET['type'] == "delete") {
            $wine = Wine::getById(intval($_GET['id']));

            if($wine->delete()) {
                $message = Message::findById('wine', 3);
            } else {
                $message = Message::findById('wine', 4);
            }
        }

    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>
    <!-- Ajouter un vin -->
    <div class="row-block justify-content-center">
        <div class="block">
            <div class="block-header">
                <h2>Ajouter un Vin</h2>
            </div>
            <div class="block-content">
                <form method="post">
                    <div class="form-group">
                        <div class="form-part">
                            <label for="name_wine">
                                <input type="text" name="name_wine" placeholder="Nom du vin" required>
                            </label>
                            <label for="description_wine">
                                <input type="text" name="description_wine" placeholder="Description du vin">
                            </label>
                        </div>
                        <div class="form-part">
                            <label for="id_igp">
                                <select name="id_igp" id="igp" required>
                                    <option value="" hidden selected disabled>Choisissez un IGP</option>
                                    <?php
                                    $igp = Appellation::getAll();

                                    foreach ($igp as $item) {
                                        echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                                    }

                                    ?>
                                </select>
                            </label>
                            <label for="degree_igp">
                                <input type="number" step="0.01" name="degree_igp" placeholder="Degrée de l'IGP" required>
                            </label>
                        </div>
                    </div>

                    <button class="btn" type="submit">Ajouter</button>
                </form>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="block-header">
            <h2>Liste/Action des Vins</h2>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th style="width: 100px;">N* Vin</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>IGP</th>
                    <th>Degrée C°</th>
                    <th style="width: 100px;">Action</th>
                </tr>
                <?php

                $wines = Wine::getAll();

                /** @var Wine $wine */
                foreach ($wines as $wine) {
                    echo "<tr>";

                    $id = $wine->getId();
                    echo "<td style='width:75px;'>" . $id . "</td>";
                    echo "<td>" . $wine->getName() . "</td>";
                    echo "<td>" . $wine->getDescription() . "</td>";
                    echo "<td>" . $wine->getAppellation()->getLibelle() . "</td>";
                    echo "<td style='width:100px;'>" . $wine->getDegree() . " C°</td>";
                    echo "<td class='td-flex'>
<a class='afa fa-blue' href='wine_edit.php?id_wine=$id' title='Éditer cette ligne'><i class='fas fa-edit'></i></a>
<a class='afa fa-red' href='wine.php?type=delete&id=$id' title='Supprimer cette ligne'><i class='fas fa-trash-alt'></i></a>
</td>";

                    echo "</tr>";
                }

                ?>
            </table>
        </div>
    </div>
</main>
</body>
</html>
