<?php

use __Wine\Accord;
use __Wine\AccordRel;
use __Wine\Bouche;
use __Wine\BoucheRel;
use __Wine\Cepage;
use __Wine\CepageRel;
use __Wine\VinificationRel;
use __Wine\Vinification;
use __Wine\RecolteRel;
use __Wine\Recolte;
use __Wine\EvolutionRel;
use __Wine\Evolution;
use __Wine\Appellation;
use __Wine\RobeRel;
use __Wine\Robe;
use __Wine\NezRel;
use __Wine\Nez;
use __Wine\Wine;

require '../../dbs/__DBS.php';

if(!need_user() && !$user->isAdmin()){
    redirect('../index.php');
}

if($_POST) {

    if(isset($_POST['type'])) {
        switch ($_POST['type']){
            case "add":
                if(isset($_POST['table'])) {
                    $table = $_POST['table'];

                    if(isset($_POST['id_info'])) {

                        $id = $_POST['id_info'];

                        $params = ["id_wine"=>$_GET['id_wine'], "id_$table"=>$id];
                        if(isset($_POST['percentage'])) {
                            $params['percentage'] = floatval($_POST['percentage']);
                        }

                        $database->insert("dbs_".$table."_rel", $params);
                        redirect('wine_edit.php?id_wine=' . $_GET['id_wine']);

                    }
                }

                break;
        }
    }




}

//Get for the delete row
if($_GET) {
    if(isset($_GET['id_wine'])) {

        if(isset($_GET['type'])) {
            switch ($_GET['type']){
                case "delete":
                    if(isset($_GET['table'])) {
						$table = $_GET['table'];
                        if (isset($_GET['id_info'])) {

                            $id = $_GET['id_info'];

                            $database->delete("dbs_" . $table . "_rel", ["id_wine" => $_GET['id_wine'], "id_$table" => $id]);
                            redirect('wine_edit.php?id_wine=' . $_GET['id_wine']);
                        }
                    }
                    break;
            }
        }

        $wine = Wine::getById($_GET['id_wine']);

    } else {
        redirect('wine.php');
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body id="admin">
<?php require 'include/_navbar.php'; ?>
<main>
    <?php
    if(isset($message)){
        echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
    }
    ?>

    <div class="block">
        <div class="block-header">
            <h3>Résumé du vin : <?php echo $wine->getName(); ?></h3>
        </div>
        <div class="block-content">
            <table>
                <tr>
                    <th>Accord</th>
                    <th>Bouche</th>
                    <th>Cepage</th>
                    <th>Evolution</th>
                    <th>Nez</th>
                    <th>Recolte</th>
                    <th>Robe</th>
                    <th>Vinification</th>
                </tr>
                <?php

                for($i = 0; $i < 10; $i++) {
                    echo "<tr>";

                    $accord =       $i < sizeof($wine->getListAccord()) ? $wine->getListAccord($i)->getAccord() : null;
                    $bouche =       $i < sizeof($wine->getListBouche()) ? $wine->getListBouche($i)->getBouche() : null;
                    $cepage =       $i < sizeof($wine->getListCepage()) ? $wine->getListCepage($i)->getCepage() : null;
                    $evolution =    $i < sizeof($wine->getListEvolution()) ? $wine->getListEvolution($i)->getEvolution() : null;
                    $nez =          $i < sizeof($wine->getListNez()) ? $wine->getListNez($i)->getNez() : null;
                    $recolte =      $i < sizeof($wine->getListRecolte()) ? $wine->getListRecolte($i)->getRecolte() : null;
                    $robe =         $i < sizeof($wine->getListRobe()) ? $wine->getListRobe($i)->getRobe() : null;
                    $vinification = $i < sizeof($wine->getListVinification()) ? $wine->getListVinification($i)->getVinification() : null;

                    if($accord != null) echo "<td>" . $accord->getLibelle() . "</td>"; else echo "<td></td>";
                    if($bouche != null) echo "<td>" . $bouche->getLibelle() . "</td>"; else echo "<td></td>";
                    if($cepage != null) echo "<td>" . $cepage->getLibelle() . "</td>"; else echo "<td></td>";
                    if($evolution != null) echo "<td>" . $evolution->getLibelle() . "</td>"; else echo "<td></td>";
                    if($nez != null) echo "<td>" . $nez->getLibelle() . "</td>"; else echo "<td></td>";
                    if($recolte != null) echo "<td>" . $recolte->getLibelle() . "</td>"; else echo "<td></td>";
                    if($robe != null) echo "<td>" . $robe->getLibelle() . "</td>"; else echo "<td></td>";
                    if($vinification != null) echo "<td>" . $vinification->getLibelle() . "</td>"; else echo "<td></td>";


                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>

    <!-- Accord -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter un Accord</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="accord">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez un accord</option>
                            <?php
                            $accords = Accord::getAll();
                            foreach ($accords as $accord) {
                                echo "<option value='".$accord->getId()."'>".$accord->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Accords</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Accord</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = AccordRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getAccord();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=accord&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Bouche -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter une Bouche</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="bouche">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez une bouche</option>
                            <?php
                            $objects = Bouche::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Bouches</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Bouche</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = BoucheRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getBouche();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=bouche&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Cepage -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter un Cepage</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="cepage">
                    </label>
                    <label for="">
                        <input type="number" step="0.01" name="percentage" placeholder="pourcentage">
                    </label>
                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez un Cepage</option>
                            <?php
                            $objects = Cepage::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Cepages</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Cepage</th>
                        <th>Libellé</th>
                        <th>Pourcentage</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = CepageRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getCepage();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td>".$item->getPercentage()."%</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=cepage&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Evolution -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter une Evolution</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="evolution">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez une evolution</option>
                            <?php
                            $objects = Evolution::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Evolutions</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Evolution</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = EvolutionRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getEvolution();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=evolution&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Nez -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter un Nez</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="nez">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez un nez</option>
                            <?php
                            $objects = Nez::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Nez</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Nez</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = NezRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getNez();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=nez&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Recolte -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter une Recolte</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="recolte">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez une recolte</option>
                            <?php
                            $objects = Recolte::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Recoltes</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Recolte</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = RecolteRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getRecolte();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=recolte&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Robe -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter une Robe</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="robe">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez une robe</option>
                            <?php
                            $objects = Robe::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Robes</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Robe</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = RobeRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getRobe();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=robe&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
    <!-- Vinification -->
    <div class="row-block">
        <div class="block block-sm">
            <div class="block-header">
                <h3>Ajouter une Vinification</h3>
            </div>
            <div class="block-content">
                <form method="post">
                    <label for="type" hidden >
                        <input type="hidden" hidden name="type" value="add">
                        <input type="hidden" hidden name="table" value="vinification">
                    </label>

                    <label for="">
                        <select name="id_info" required>
                            <option value="" hidden selected>Choisissez une vinification</option>
                            <?php
                            $objects = Vinification::getAll();
                            foreach ($objects as $item) {
                                echo "<option value='".$item->getId()."'>".$item->getLibelle()."</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <button class="btn">Ajouter</button>
                </form>
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3>Liste/Actions des Vinifications</h3>
            </div>
            <div class="block-content">
                <table>
                    <tr>
                        <th style="width:100px;">N* Vinification</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    <?php

                    $list = VinificationRel::findById(['id_wine'=>$wine->getId()]);

                    foreach ($list as $item){
                        echo "<tr>";

                        $object = $item->getVinification();
                        echo "<td>".$object->getId()."</td>";
                        echo "<td>".$object->getLibelle()."</td>";
                        echo "<td><a class='afa fa-red' href='wine_edit.php?id_wine=".$wine->getId()."&type=delete&table=vinification&id_info=".$object->getId()."'><i class='fas fa-trash'></i></a></td>";
                        echo "</tr>";
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>
