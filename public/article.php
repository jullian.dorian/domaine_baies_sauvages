<?php

require '../dbs/__DBS.php';

need_maintenance();

if($_GET){
    if(isset($_GET['token'])) {
        $article = \__Item\Item::getByToken(htmlspecialchars($_GET['token']));

        if($article == null)
            redirect('caveau.php');

        $wine = $article->getWine();
        $conditionnement = $article->getConditionnement()->getLibelle();
        $picture = $article->getPicture();


    } else {
        redirect('caveau.php');
        exit();
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
<?php include('include/_navbar.php'); ?>
<main id="home">
    <section class="home-block">
        <div class="container">
            <h1 class="article-title">Caveau > <?= $article->getLibelle();?></h1>
            <div class="block-article">
                <div class="block-header">
                    <div class="block-left">
                        <img class="picture" src="<?php echo $picture->getUrl();?>" alt="<?php echo $picture->getName();?>">
                    </div>
                    <div class="block-right">
                        <div class="details">
                            <?= $article->getDetails(); ?>
                        </div>
                        <div class="infos">
                            <?= $wine->getInfos(); ?>
                        </div>
                    </div>

                </div>
                <div class="block-content">
                    <div class="box" ><span class="box-underground"></span></div>
                    <div class="container">
                        <h2><?= $article->getLibelle(); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include('include/_footer.php'); ?>
</body>
</html>