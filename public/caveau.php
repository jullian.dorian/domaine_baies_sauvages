<?php

require '../dbs/__DBS.php';

need_maintenance();

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
<?php include('include/_navbar.php'); ?>
<main id="home">
    <?php
    $articles = \__Item\Item::getAll();
    $size = sizeof($articles);

    if($size != 0) {
        ?>
        <section class="home-block"
                 style="height: auto; background: url('assets/background-wines.jpg') no-repeat fixed;background-size: 100%;">
            <header>
                <h3 class="container">Caveau ></h3>
            </header>
            <div class="container">

                    <?php

                    $c = 0;

                    for($i = 0; $i < ceil($size/3); $i++) {

                        if ($c % 3 == 0) echo "<div class='row-block'>";

                        /** @var \__Item\Item $article */
                        for($j = 0; $j < 3; $j++) {

                            if(!isset($articles[$j + ($i * 3)]))
                                break;

                            $article = $articles[$j + ($i * 3)];

                            echo "<div class='item-block block-sm'>";
                            echo "<div class='item-block-header'>";
                            echo "<img src='" . $article->getPicture()->getUrl() . "' />";
                            echo "<span class='box-underground'></span>";
                            echo "<h3 class='title'>" . $article->getLibelle() . "</h3>";
                            echo "</div>";
                            echo "<div class='item-block-content'>";
                            echo "<p>" . htmlspecialchars_decode($article->getDescription()) . "</p>";
                            echo "<p class='gold align-text-center'>Prix à l'unité : " . $article->getPrice() . "€</p>";
                            echo "<p class='gold align-text-center'>Promotion : " . ($article->getPromotion() != 0 ? round($article->getPromotion() * 100)."%" : "Aucune") . "</p>";
                            echo "</div>";
                            echo "<div class='item-block-footer'>";
                            echo "<a class='btn' href='article.php?token=" . $article->getToken() . "'>En savoir plus</a>";
                            echo "</div>";
                            echo "</div>";

                            $c++;
                        }

                        if($c % 3 == 0) echo "</div>";
                    }

                    ?>
            </div>
        </section>
        <?php
    }
    ?>
</main>
<?php include('include/_footer.php'); ?>
</body>
</html>