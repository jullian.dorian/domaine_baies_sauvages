<footer>
    <div class="container">
        <div id="footer-logo">
            <img src="/assets/icon/logo.svg" alt="Logo">
        </div>
        <div class="flex-block">
            <div class="flex-item">
                <h3>Adresse Postale</h3>
                <p>1 rue des amayrols</p>
                <p>34320 Nizas</p>
            </div>
            <div class="flex-item">
                <h3>Propriétaire</h3>
                <p>Emmanuel Thalic</p>
                <p>0609556568</p>
                <p>0467253395</p>
                <p>veronique.thalic@gmail.com</p>
            </div>
            <div class="flex-item">
                <h3>Administrateur</h3>
                <p>Jullian--Cambon Dorian</p>
                <p>jullian.dorian@gmail.com</p>
            </div>
        </div>
        <div id="copyright">
            <p>Tous Droits Reservés - 2018 - <a href="">Conditions Générales d'Utilisations</a> - <a href="">Conditions Générales de Ventes</a></p>
        </div>
    </div>
</footer>