<header id="navbar">
    <div id="navbar-top">
        <div class="container">
            <div class="flex-block">
                <div class="flex-item">
                    <h2><b>Domaine les Baies Sauvages</b></h2>
                </div>
                <?php
                if(is_null($user)) {
                ?>
                <div class="flex-block flex-align-right">
                    <a class="btn-separator" href="/register/">Créer un compte</a>
                    <a href="/login/">Se connecter</a>
                </div>
                <?php } else { ?>
                <div class="flex-block flex-align-right">
                    <a href="/profil/" class="btn-separator">Mon Compte</a>
                    <a href="/profil/logout.php">Se déconnecter</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="navbar-items">
        <div class="container">
            <div class="flex-block">
                <div id="navbar-left-part">
                    <div class="flex-block">
                        <a class="flex-item" href="/">Accueil</a>
                        <a class="flex-item" href="">Histoire du domaine</a>
                        <a class="flex-item" href="">Les Cépages</a>
                    </div>
                </div>
                <div id="navbar-mid-part">
                    <img class="icon" src="/assets/icon/logo.svg" alt="Logo">
                    <span class="box-underground"></span>
                </div>
                <div id="navbar-right-part">
                    <div class="flex-block">
                        <a class="flex-item" href="caveau.php">Le Caveau</a>
                        <a class="flex-item" href="">Tarifs et Commandes</a>
                        <a class="flex-item" href="">Atelier pédagogique</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>