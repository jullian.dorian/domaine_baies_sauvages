carrousel = {
    current: null,
    current_img: null,
    current_ind: 0,
    total_diapo: 0,
    interval: null,
    init: function(){

        let current_img = document.getElementById("__img-0");
        let current_box = document.getElementById("__box-0");

        if(current_box != null && current_img != null) {
            this.current = current_box;
            this.current_img = current_img;
        }

        for($i = 0; $i < 10; $i++){
            if(document.getElementById("__img-"+$i) != null)
                this.total_diapo++;
        }

        this.interval = function() {
            setInterval(this.next(), 5000);
        };

    },
    changeTo: function($index) {

        if($index >= 0 && $index < 10) {

            if($index != this.current_ind) {

                let new_img = document.getElementById("__img-"+$index);
                let new_doc = document.getElementById("__box-"+$index);

                if(new_doc != null){
                    new_doc.classList.add("active");

                    new_img.classList.remove("hide");
                    new_img.classList.add("visible");

                    this.current.classList.remove("active");

                    this.current_img.classList.remove("visible");
                    this.current_img.classList.add("hide");

                    this.current = new_doc;
                    this.current_img = new_img;
                    this.current_ind = $index;

                    clearInterval(this.interval);
                    this.interval = function() {
                        setInterval(this.next(), 5000);
                    };
                }

            }

        }
    },
    next: function(){

        $ind = this.current_ind;

        $ind++;

        if($ind > this.total_diapo) {
            this.current_ind = 0;
            this.changeTo(0);
        } else {
            this.changeTo($ind);
        }


    }
};

document.body.onload = function(){
    carrousel.init();
};
