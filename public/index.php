<?php

use __Picture\Diaporama;

require '../dbs/__DBS.php';

need_maintenance();

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
<?php include('include/_navbar.php'); ?>
<main id="home">
    <section class="home-block min-home-block">
        <div class="diaporama">
            <?php
            $diapos = Diaporama::getAll();

            $size = sizeof($diapos);
            $size_box = 0;

            if($diapos != null && $size != 0) {
                for($i = 0; $i < $size; $i++){
                    /** @var Diaporama $diapo */
                    $diapo = $diapos[$i];

                    if($diapo != null && $diapo->getPicture() != null) {

                        if ($diapo->isShow() && $i == 0) {
                            $size_box++;
                            echo "<img id='__img-$i' class='visible' src='" . $diapo->getPicture()->getUrl() . "' alt='Image diaporama' />";
                        } else if ($diapo->isShow()) {
                            $size_box++;
                            echo "<img id='__img-$i' class='hide' src='" . $diapo->getPicture()->getUrl() . "' alt='Image diaporama' />";
                        }
                    }
                }

                echo "<div class='diaporama-bar'>";
                for ($i = 0; $i < $size_box; $i++) {
                    if($i == 0)
                        echo "<span class='diaporama-pointer active' id='__box-$i' onclick='carrousel.changeTo($i);'></span>";
                    else
                        echo "<span class='diaporama-pointer' id='__box-$i' onclick='carrousel.changeTo($i);'></span>";
                }
                echo "</div>";
            } else {
                echo "<img alt='Default image' class='visible' src='/assets/background-wines.jpg'/>";
                echo "<span style='position: absolute; left: 50%; transform:translateX(-50%); display:block; top:50%; background: white; padding: 30px; font-size:35pt;'>Aucun diaporama</span>";
            }

            ?>
        </div>
    </section>
    <section class="home-block">
        <header>
            <h3 class="container">Les animations du mois</h3>
        </header>
        <div class="container">
            //Evenement
        </div>
    </section>
    <?php
    $articles = \__Item\Item::getLast(3);
    $size = sizeof($articles);

    if($size != 0) {
        ?>
        <section class="home-block"
                 style="background: url('assets/background-wines.jpg') no-repeat; background-size: 100%;">
            <header>
                <h3 class="container">Les vins du moment</h3>
            </header>
            <div class="container">
                <div class="row-block">
                    <?php

                    /** @var \__Item\Item $article */
                    foreach ($articles as $article) {

                        echo "<div class='item-block'>";
                            echo "<div class='item-block-header'>";
                                echo "<img src='".$article->getPicture()->getUrl()."' />";
                                echo "<span class='box-underground'></span>";
                                echo "<h3 class='title'>".$article->getLibelle()."</h3>";
                            echo "</div>";
                            echo "<div class='item-block-content'>";
                                echo "<p>".htmlspecialchars_decode($article->getDescription())."</p>";
                                echo "<p class='gold align-text-center'>Prix à l'unité : ".$article->getPrice()."€</p>";
                                echo "<p class='gold align-text-center'>Promotion : ".($article->getPromotion() != 0 ? round($article->getPromotion()*100)."%" : "Aucune")."</p>";
                            echo "</div>";
                            echo "<div class='item-block-footer'>";
                                echo "<a class='btn' href='article.php?token=".$article->getToken()."'>En savoir plus</a>";
                            echo "</div>";                        echo "</div>";

                    }

                    ?>
                </div>
            </div>
        </section>
        <?php
    }
    ?>
    <script src="include/app.js" async></script>
</main>
<?php include('include/_footer.php'); ?>
</body>
</html>