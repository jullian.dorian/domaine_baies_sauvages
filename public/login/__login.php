<?php

require '../../dbs/__DBS.php';

if(need_user() || is_null($_POST) || empty($_POST)){
    redirect('/index.php');
}

if($_POST){

    $email = htmlspecialchars($_POST['email']);
    $password = sha1(htmlspecialchars($_POST['password']));

    $loginUser = new \__User\LoginUser($email, $password);

    if($loginUser->exist()){
        $user = $loginUser->connect();
        $user->saveSession();
        redirect('/profil');
    } else {
        redirect('index.php?msg_id=1');
    }

}

?>