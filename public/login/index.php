<?php

use __Message\Message;

require '../../dbs/__DBS.php';

if(need_user()){
    redirect('../images.php');
}

if($_GET) {
    if(isset($_GET['msg_id'])){
        $message = Message::findById("connection", $_GET['msg_id']);
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="../style/style.css" type="text/css">
</head>
<body>
<?php include('../include/_navbar.php'); ?>

<main>
    <div class="container">
        <?php
        if(isset($message)){
            echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
        }
        ?>
        <form action="__login.php" method="post">
            <header>
                <h3>Connectez vous</h3>
                <p>Entrez vos informations pour accéder à votre compte.</p>
            </header>
            <div class="form-part">
                <label for="email">
                    <input placeholder="Votre email" type="email" name="email" required>
                </label>
                <label for="password">
                    <input placeholder="Votre mot de passe" minlength="8" maxlength="32" type="password" name="password" required>
                </label>
            </div>
            <button class="btn" type="submit">Se connecter</button>
        </form>
    </div>
</main>

<?php include('../include/_footer.php'); ?>

</body>
</html>