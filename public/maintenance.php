<?php

require '../dbs/__DBS.php';

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body id="maintenance">
<div class="container">
    <h1>Site en maintenance</h1>
</div>

</body>
</html>