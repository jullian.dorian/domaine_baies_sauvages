<?php

require '../../dbs/__DBS.php';

need_maintenance();

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="../style/style.css" type="text/css">
</head>
<body>
<?php include('../include/_navbar.php'); ?>
<main>
    <div class="container">
        <div class="menu">
            <header class="menu-header">
                <div class="menu-head">
                    <h2>Mon Compte</h2>
                    <p>Consultez votre profil selon les différents onglets.</p>
                </div>
            </header>
            <div class="menu-wrapper">
                <div class="menu-nav">
                    <ul>
                        <li><a href="">Mon compte</a></li>
                        <li><a href="">Mes commandes</a></li>
                        <li><a href="">Mes paramètres</a></li>
                        <?php if($user->isAdmin()) echo "<li><a style='color: #ff4449;' href='/admin'>Administration</a></li>"; ?>
                    </ul>
                </div>
                <div class="menu-content">
                    <?php
                    echo $user->getName();
                    ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include('../include/_footer.php'); ?>
</body>
</html>
