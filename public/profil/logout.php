<?php

require '../../dbs/__DBS.php';

unset($_SESSION['user']);
session_destroy();
redirect('../index.php');
