<?php

use __User\RegisterUser;

require '../../dbs/__DBS.php';

if(need_user() || is_null($_POST) || empty($_POST)){
    redirect('../index.php');
}

if($_POST){

    $email = htmlspecialchars($_POST['email']);
    $password = sha1(htmlspecialchars($_POST['password']));
    $password2 = sha1(htmlspecialchars($_POST['password2']));
    $name = htmlspecialchars($_POST['name']);
    $surname = htmlspecialchars($_POST['surname']);
    $age = intval($_POST['age']);
    $telephone = htmlspecialchars($_POST['telephone']);
    $address = htmlspecialchars($_POST['address']);
    $client = isset($_POST['client']) ? true : false;

    $registerUser = new RegisterUser($email, $password, $password2, $name, $surname, $age, $address, $telephone, $client);

    if($registerUser->passwordEquals()){
        if(!$registerUser->exist()){
            $user = $registerUser->create();
            $user->saveSession();
            redirect('/profil');
        } else {
            redirect('index.php?msg_id=1');
        }
    } else {
        redirect('index.php?msg_id=2');
    }

}

?>