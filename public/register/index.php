<?php

use __Message\Message;

require '../../dbs/__DBS.php';

if(need_user()){
    redirect('../index.php');
}

need_maintenance();

if($_GET) {
    if(isset($_GET['msg_id'])){
        $message = Message::findById("inscription", $_GET['msg_id']);
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <title>Domaine les Baies Sauvages</title>
    <link rel="stylesheet" href="../style/style.css" type="text/css">
</head>
<body>
<?php include('../include/_navbar.php'); ?>

<main>
    <div class="container">
        <?php
        if(isset($message)){
            echo "<div class='msg ".$message['type']."'>" . $message['msg'] . "</div>";
        }
        ?>
        <form action="__register.php" method="post">
            <header>
                <h3>Créer un compte sur notre site</h3>
                <p>En créant un compte vous acceptez les conditions générales d'utilisations et de ventes.</p>
            </header>
            <div class="form-group">
                <div class="form-part">
                    <label for="email">
                        <input placeholder="Votre email" type="email" name="email" required>
                    </label>
                    <label for="password">
                        <input placeholder="Votre mot de passe" minlength="8" maxlength="32" type="password" name="password" required>
                    </label>
                    <label for="password">
                        <input placeholder="Répétez le mot de passe" minlength="8" maxlength="32" type="password" name="password2" required>
                    </label>
                    <label for="telephone">
                        <input placeholder="Votre téléphone" type="tel" name="telephone" minlength="10" required>
                    </label>
                </div>
                <div class="form-part">
                    <label for="name">
                        <input placeholder="Votre nom de famille" pattern="[a-zA-Z-]+" type="text" name="name" required>
                    </label>
                    <label for="surname">
                        <input placeholder="Votre prénom" type="text" pattern="[a-zA-Z-]+" name="surname" required>
                    </label>
                    <label for="Age">
                        <input placeholder="Votre âge" min="0" max="200" type="number" name="age" required>
                    </label>
                    <label for="address">
                        <input placeholder="Votre adresse postale" type="text" name="address" required>
                    </label>
                </div>
            </div>
            <label class="form-large" for="password">
                <b>Déjà client :</b>
                <span>En dehors du site internet, avez vous déjà acheté sur ce domaine ?</span>
                <span>(Laissez vide si ce n'est pas le cas)</span>
                <input type="checkbox" name="client">
            </label>
            <button class="btn" type="submit">Créer mon compte</button>
        </form>
    </div>
</main>
<?php include('../include/_footer.php'); ?>
</body>
</html>