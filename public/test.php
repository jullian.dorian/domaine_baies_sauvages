<?php

require '../dbs/__DBS.php';

if(!\__DBS\need_user()) {
    \__DBS\redirect('index.php');
}

$token = $_GET['token'];

$article = \__DBS__Class\Item::getByToken($token);

if($article == null)
    \__DBS\redirect('caveau.php');

if($_POST){
    if(isset($_POST['qtt'])) {
        $qtt = intval($_POST['qtt']);

        //New order
        $request = $database->insert("dbs_order", ['token'=>\__DBS__Class\Utils::generate_token(),
            'id_user'=>$user->getId(), 'date_creation'=>date('Y-m-d')]);

        if($request->isSuccess()) {
            $last_id = $request->getLastId();

            //Add article
            $request = $database->insert("dbs_contains", ['quantity'=>$qtt,'id_item'=>$article->getId(),
                'id_order'=>$last_id]);

            if($request->isSuccess()) {
                echo "Commande crée et article ajoutée";
            } else {
                echo "Erreur article";
            }

        } else {
            echo "Erreur d'order";
        }
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="post">
    <?php
    echo "Choisir la quantité pour l'article : " . $article->getLibelle();
    ?>
    <label for="">
        <input placeholder="Entrez la quantité" type="number" step="1" name="qtt">
    </label>
    <button type="submit">Créer</button>
</form>
</body>
</html>
